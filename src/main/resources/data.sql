create table if not exists address(id bigint primary key,
            		house_number varchar(100), 
            		street varchar(100), 
            		city varchar(30), 
            		postcode varchar(10));

create table if not exists site(id bigint primary key,
                    site_name varchar(100),
            		longitude double , 
            		latitude double, 
            		address_id bigint, 
            		is_external varchar(1));
            		
create table if not exists department(id bigint primary key,
                    department_code varchar(30),
            		department_name  varchar(100),
            		parent_id bigint,
            		manager_id bigint,
            		manager_name varchar(200));

create table if not exists employee(id bigint primary key,
					employee_no varchar(20),
 					ssn varchar(50),
 					first_name varchar(100),
 					last_name varchar(100),
 					address_id bigint,
 					email varchar(100),
 					business_fax_number varchar(20),
 					business_phone_number varchar(15),
 					gender varchar(1),
 					dob date,
 					telephone_number varchar(15),
 					mobile_number varchar(15),
 					fax_number varchar(20),
 					entry_date date,
 					leaving_date date,
 					is_external varchar(1));
 
create table if not exists employee_department(id bigint primary key,
                    employee_id bigint,
            		department_id  bigint,
            		is_manager varchar(1),
            		start_date date,
            		end_date date);
            		
create table if not exists employee_site(id bigint primary key,
                    employee_id bigint,
            		site_id  bigint,
            		start_date date,
            		end_date date);   
            		
insert into address(id, house_number,street, city, postcode) values(1, 'No.4 2nd floor Star Building', 'Bolz strasse', 'stuttgart', '70173');
insert into address(id, house_number,street, city, postcode) values(2, 'No.10 13th floor Planet Building', 'Herm strasse', 'essen', '34500');
insert into address(id, house_number,street, city, postcode) values(3, '42 Pacific Tower', 'Mahingiwa', 'Makati', '342343');
insert into address(id, house_number,street, city, postcode) values(4, '40A', 'Maayusin st', 'Quezon', '4567');
insert into address(id, house_number,street, city, postcode) values(5, '102B', 'Katipunan st', 'Quezon', '34298');
insert into address(id, house_number,street, city, postcode) values(6, '103', 'SM North', 'Lololos', '4391');
insert into address(id, house_number,street, city, postcode) values(7, '408 Book Building', 'C5 st', 'Ortigas', '66823');
insert into address(id, house_number,street, city, postcode) values(8, '80A', 'TV Building', 'Hanover', '3393');

insert into site(id,site_name,longitude,latitude,address_id,is_external) values(1, 'Stuttgart', 123.34, 56.67, 1, 'Y');
insert into site(id,site_name,longitude,latitude,address_id,is_external) values(2, 'Essen', 46.84, 180.23, 2, 'Y');
insert into site(id,site_name,longitude,latitude,address_id,is_external) values(3, 'Hanover', 1436.84, 2280.23, 8, 'Y');

insert into employee(id, employee_no,ssn,first_name,last_name,address_id,email,business_fax_number,business_phone_number,gender,dob,telephone_number,mobile_number,fax_number,entry_date,leaving_date,is_external) values(1, 'EMP00001', '111', 'Hong', 'Nguyen', 3, 'hong@gmail.com', '23213', '2132132', 'F', '1980-03-10', null, null, null, '2008-01-01','2020-12-31', 'N');
insert into employee(id, employee_no,ssn,first_name,last_name,address_id,email,business_fax_number,business_phone_number,gender,dob,telephone_number,mobile_number,fax_number,entry_date,leaving_date,is_external) values(2, 'EMP00002', '222', 'Hoo', 'Chooseng', 4, 'hoh@gmail.com', '222222', '232323', 'M', '1956-03-10', null, null, null, '2005-02-10','2020-12-31', 'N');
insert into employee(id, employee_no,ssn,first_name,last_name,address_id,email,business_fax_number,business_phone_number,gender,dob,telephone_number,mobile_number,fax_number,entry_date,leaving_date,is_external) values(3, 'EMP00003', '333', 'Thed', 'Dr', 5, 'thed@gmail.com', '333333', '333333', 'M', '1950-06-10', null, null, null, '2000-02-10','2020-12-31', 'N');
insert into employee(id, employee_no,ssn,first_name,last_name,address_id,email,business_fax_number,business_phone_number,gender,dob,telephone_number,mobile_number,fax_number,entry_date,leaving_date,is_external) values(4, 'EMP00004', '444', 'Chew', 'Ming', 6, 'chew@gmail.com', '44444', '44444', 'F', '1955-06-10', null, null, null, '2015-02-03','2020-12-31', 'N');
insert into employee(id, employee_no,ssn,first_name,last_name,address_id,email,business_fax_number,business_phone_number,gender,dob,telephone_number,mobile_number,fax_number,entry_date,leaving_date,is_external) values(5, 'EMP00005', '555', 'Wilson', 'Chan', 7, 'wilson@gmail.com', '55555', '555555', 'M', '1955-06-10', null, null, null, '2016-01-01','2020-12-31', 'N');

insert into department(id, department_code,department_name,parent_id,manager_id, manager_name) values(1, 'CMGG', 'CMG Group', null, 3, 'Thed Dr');
insert into department(id, department_code,department_name,parent_id,manager_id, manager_name) values(2, 'CMGO', 'CMG Online', 1, 2, 'Hoo Chooseng');
insert into department(id, department_code,department_name,parent_id,manager_id, manager_name) values(3, 'CMGR', 'CMG Research', 1, null, null);

insert into employee_department(id, employee_id,department_id,is_manager,start_date,end_date) values(1, 3, 2, 'Y', '2000-01-01', '2009-12-31');
insert into employee_department(id, employee_id,department_id,is_manager,start_date,end_date) values(2, 2, 2, 'Y', '2010-01-01', '2018-01-01');
insert into employee_department(id, employee_id,department_id,is_manager,start_date,end_date) values(3, 1, 1, 'Y', '2010-01-01', '2018-01-01');
insert into employee_department(id, employee_id,department_id,is_manager,start_date,end_date) values(4, 1, 2, 'Y', '2008-01-01', '2009-12-31');
insert into employee_department(id, employee_id,department_id,is_manager,start_date,end_date) values(5, 1, 2, 'N', '2010-01-01', '2018-01-01');
insert into employee_department(id, employee_id,department_id,is_manager,start_date,end_date) values(6, 4, 2, 'N', '2015-02-03', '2015-11-03');
insert into employee_department(id, employee_id,department_id,is_manager,start_date,end_date) values(7, 5, 2, 'N', '2016-01-01', '2018-01-01');

insert into employee_site(id, employee_id, site_id,start_date,end_date) values(1, 1,1, '2010-05-10', '2015-10-10');
insert into employee_site(id, employee_id, site_id,start_date,end_date) values(2, 2,1, '2015-05-10', '2019-01-01');
insert into employee_site(id, employee_id, site_id,start_date,end_date) values(3, 3,1, '2010-05-10', '2019-01-01');
insert into employee_site(id, employee_id, site_id,start_date,end_date) values(4, 4,2, '2015-02-03', '2019-01-01');
insert into employee_site(id, employee_id, site_id,start_date,end_date) values(5, 5,3, '2016-01-01', '2019-01-01');