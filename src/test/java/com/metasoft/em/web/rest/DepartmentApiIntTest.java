package com.metasoft.em.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.hamcrest.Matchers.hasSize;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.metasoft.em.AbstractIntegrationTest;
import com.metasoft.em.service.DepartmentService;
import com.metasoft.em.service.EmployeeService;

/**
 * The Class DepartmentApiIntTest.
 *
 * @author Alex Dang
 */
public class DepartmentApiIntTest extends AbstractIntegrationTest {
	
	/** The department service. */
	@Autowired
	private DepartmentService departmentService;
	
	/** The employee service. */
	@Autowired
	private EmployeeService employeeService;
	
	/** The rest department api mock mvc. */
	private MockMvc restDepartmentApiMockMvc;
	
	/**
	 * This method is used to prepare MockMvc to test Restful web service.
	 */
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		DepartmentApi mockDepartmentApi = new DepartmentApi();
		ReflectionTestUtils.setField(mockDepartmentApi, "departmentService", departmentService);
		ReflectionTestUtils.setField(mockDepartmentApi, "employeeService", employeeService);
		
		restDepartmentApiMockMvc = MockMvcBuilders.standaloneSetup(mockDepartmentApi).build();
	}
	
	/**
	 * This method is used to test find Current Manager By Department
	 * the url can be: http://localhost:5000/api/v1/departments/CMGO/employees/current/manager.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFindCurrentManagerByDepartmentCode_forCMGO_thenHooChooseng() throws Exception {
		
		String cmgoUrlPath = 
					DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
											.concat("/employees/current/manager");
		
		//this method expect to get current manager is Hoo Choseng: EMP00002
		restDepartmentApiMockMvc.perform(get(cmgoUrlPath)
								.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(status().isOk())
								.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(jsonPath("$.employeeNo").value(HOO_EMPLOYEE_NO));
		
	}
	
	/**
	 * This method is used to test find Current Manager By Department for
	 * CMGR Department, as CMGR department doesn't have manager now, it will return
	 * NO_CONTENT
	 * the url can be: http://localhost:5000/api/v1/departments/CMGR/employees/current/manager.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFindCurrentManagerByDepartmentCode_forCMGR_thenNO_CONTENT() throws Exception {
		
		String cmgrUrlPath = 
					DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGR_DEPARTMENT_CODE)
											.concat("/employees/current/manager");
		
		//we don't have CMGL so expect to return NO_CONTENT: 204 
		restDepartmentApiMockMvc.perform(get(cmgrUrlPath)
								.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(status().isNoContent());
		
	}
	
	
	
	/**
	 * Test find manager for CMGO at date 2012-03-03
	 * http://localhost:5000/api/v1/departments/{departmentCode}/employees/manager/{atDate}.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFindManagerByDepartmentCodeAtDate_forCMGOatDate2013_03_03_thenHooChooseng() throws Exception {
		
		String cmgo2012_03_03UrlPath = 
				DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
										.concat("/employees/manager/2012-03-03");
				
		restDepartmentApiMockMvc.perform(get(cmgo2012_03_03UrlPath)
								.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(status().isOk())
								.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(jsonPath("$.employeeNo").value(HOO_EMPLOYEE_NO));
	}
	
	
	/**
	 * Test find manager for CMGO at date 1999-01-01
	 * http://localhost:5000/api/v1/departments/{departmentCode}/employees/manager/{atDate}.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFindManagerByDepartmentCodeAtDate_forCMGOatDate1999_01_01_thenNO_CONTENT() throws Exception {
		
		String cmgo1999_01_01UrlPath = 
				DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
										.concat("/employees/manager/1999-01-01");
				
		restDepartmentApiMockMvc.perform(get(cmgo1999_01_01UrlPath)
								.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(status().isNoContent());
	}
	
	/**
	 * Test find manager for CMGO at date 2001-01-01
	 * http://localhost:5000/api/v1/departments/{departmentCode}/employees/manager/{atDate}.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFindManagerByDepartmentCodeAtDate_forCMGOatDate2001_01_01_thenThedDrEmployee() throws Exception {
		
		String cmgo2001_01_01UrlPath = 
				DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
										.concat("/employees/manager/2001-01-01");
				
		restDepartmentApiMockMvc.perform(get(cmgo2001_01_01UrlPath)
								.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(status().isOk())
								.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(jsonPath("$.employeeNo").value(DR_THED_EMPLOYEE_NO));
	}
	
	/**
	 * Test find manager for CMGO at wrong format date 2016-07-01234
	 * http://localhost:5000/api/v1/departments/{departmentCode}/employees/manager/{atDate}.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFindManagerByDepartmentCodeAtDate_forCMGOWrongDateFormat2016_07_1234_thenBAD_REQUEST() throws Exception {
		
		String cmgo2016_07_1234UrlPath = 
				DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
										.concat("/employees/manager/2016-07-1234");
				
		restDepartmentApiMockMvc.perform(get(cmgo2016_07_1234UrlPath)
								.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(status().isBadRequest());
	}
	
	
	/**
	 * Test find current employees by department code.
	 *  1. Department code: CMGO
	 *	2. Department CMGO has 4 employees are: Hoo, Wilson, Hong, Chew, Thed
	 *	3. Thed was manager for CMGO from: 2000-01-01 to 2009-12-31
	 *	4. Hoo Chooseng: from 2010-01-01 to present
	 *	5. Hong is manager from: 2008-01-01 to 2009-12-31
	 *	6. Hong is employee from: 2010-01-01 to present
	 *	7. Wilson is employee from 2016-01-01 to present
	 *	8. Chew was belonged to department CMGO from 2015-02-03 to 2015-11-03 
	 * http://localhost:5000/api/v1/departments/{departmentCode}/employees/current
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFindCurrentEmployeesByDepartmentCode_forCMGO_then3Employees_Hoo_Wilson_Hong() throws Exception {
		String cmgoUrlPath = 
				DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
										.concat("/employees/current");
		restDepartmentApiMockMvc.perform(get(cmgoUrlPath)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", hasSize(3)))
				.andExpect(jsonPath("$[0].employeeNo").value(HOO_EMPLOYEE_NO))
				.andExpect(jsonPath("$[1].employeeNo").value(HONG_EMPLOYEE_NO))
				.andExpect(jsonPath("$[2].employeeNo").value(WILSON_EMPLOYEE_NO));
	}
	
	
	/**
	 * Test find employees by department code at date 2015-07-03
	 *  1. Department code: CMGO
	 *	2. Department CMGO has 4 employees are: Hoo, Wilson, Hong, Chew, Thed
	 *	3. Thed was manager for CMGO from: 2000-01-01 to 2009-12-31
	 *	4. Chooseng: from 2010-01-01 to present
	 *	5. Hong is manager from: 2008-01-01 to 2009-12-31
	 *	6. Hong is employee from: 2010-01-01 to present
	 *	7. Wilson is employee from 2016-01-01 to present
	 *	8. Chew was belonged to department CMGO from 2015-02-03 to 2015-11-03
	 *  http://localhost:5000/api/v1/departments/{departmentCode}/employees/{atDate}
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFindEmployeesByDepartmentCodeAtDate_forCMGO_2015_07_03_returnHoo_Hong_Chew() throws Exception {
		String cmgo2015_07_03UrlPath = 
				DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
										.concat("/employees/2015-07-03");
		restDepartmentApiMockMvc.perform(get(cmgo2015_07_03UrlPath)
								.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(status().isOk())
								.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(jsonPath("$", hasSize(3)))
								.andExpect(jsonPath("$[0].employeeNo").value(HOO_EMPLOYEE_NO))
								.andExpect(jsonPath("$[1].employeeNo").value(HONG_EMPLOYEE_NO))
								.andExpect(jsonPath("$[2].employeeNo").value(CHEW_EMPLOYEE_NO));
	}
	
	/**
	 * Test find employees by department code at date 1999-01-01
	 *  1. Department code: CMGO
	 *	2. Department CMGO has 4 employees are: Hoo, Wilson, Hong, Chew, Thed
	 *	3. Thed was manager for CMGO from: 2000-01-01 to 2009-12-31
	 *	4. Hoo Chooseng: from 2010-01-01 to present
	 *	5. Hong is manager from: 2008-01-01 to 2009-12-31
	 *	6. Hong is employee from: 2010-01-01 to present
	 *	7. Wilson is employee from 2016-01-01 to present
	 *	8. Chew was belonged to department CMGO from 2015-02-03 to 2015-11-03
	 *  http://localhost:5000/api/v1/departments/{departmentCode}/employees/{atDate}
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFindEmployeesByDepartmentCodeAtDate_forCMGO_1999_01_01_thenNO_CONTENT() throws Exception {
		String cmgo1999_01_01UrlPath = 
				DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
										.concat("/employees/1999-01-01");
		restDepartmentApiMockMvc.perform(get(cmgo1999_01_01UrlPath)
								.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(status().isNoContent());
	}
	
	
	/**
	 * Test find employees by department code at date 2007-07-19
	 *  1. Department code: CMGO
	 *	2. Department CMGO has 4 employees are: Hoo, Wilson, Hong, Chew, Thed
	 *	3. Thed was manager for CMGO from: 2000-01-01 to 2009-12-31
	 *	4. Hoo Chooseng: from 2010-01-01 to present
	 *	5. Hong is manager from: 2008-01-01 to 2009-12-31
	 *	6. Hong is employee from: 2010-01-01 to present
	 *	7. Wilson is employee from 2016-01-01 to present
	 *	8. Chew was belonged to department CMGO from 2015-02-03 to 2015-11-03 
	 *  http://localhost:5000/api/v1/departments/{departmentCode}/employees/{atDate}
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFindEmployeesByDepartmentCodeAtDate_forCMGO_2007_17_19_returnThed() throws Exception {
		String cmgo2007_07_19UrlPath = 
				DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
										.concat("/employees/2007-07-19");
		restDepartmentApiMockMvc.perform(get(cmgo2007_07_19UrlPath)
								.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(status().isOk())
								.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(jsonPath("$", hasSize(1)))
								.andExpect(jsonPath("$[0].employeeNo").value(DR_THED_EMPLOYEE_NO));
	}
	
	
	/**
	 * Test find employees by department code at date 2016-07-1934
	 *  1.	Department code: CMGO
	 *	2.	Department CMGO has 4 employees are: Hoo, Wilson, Hong, Chew
	 *	3.	Hoo Chooseng: from 2010-01-01 to present
	 *	4.	Hong is manager from: 2008-01-01 to 2009-12-31
	 *	5.	Hong is employee from: 2010-01-01 to present
	 *	6.	Wilson is employee from 2016-01-01 to present
	 *	7. Chew was belonged to department CMGO from 2015-02-03 to 2015-11-03
	 *  http://localhost:5000/api/v1/departments/{departmentCode}/employees/{atDate}
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFindEmployeesByDepartmentCodeAtDate_forCMGOWrongFormatDate_thenBAD_REQUEST() throws Exception {
		String cmgo2016_07_1934UrlPath = 
				DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
										.concat("/employees/2016-07-1934");
		restDepartmentApiMockMvc.perform(get(cmgo2016_07_1934UrlPath)
								.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
								.andExpect(status().isBadRequest());
	}
	
	/**
	 * test api for isEmployeeCurrentlyAssignedToDepartment
	 * http://localhost:5000/api/v1/department/{departmentCode}/employees/{employeeNo}/isCurrentAssigned
	 * 
	 * Employee Hoo is working to CMGO from 2010-01-01 to present
	 * Employee Thed doesn't work for CMGO from 2010-01-01.
	 *
	 * @throws Exception the exception
	 */
	
	@Test
	public void testIsEmployeeCurrentlyAssignedToDepartment_forHooToCMGO_thenTrue() throws Exception {
		//expect hoo is currently is assigned to CMGO
		String hooUrlPath = 
				DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
										.concat("/employees/" + HOO_EMPLOYEE_NO + "/isCurrentAssigned");
		
		restDepartmentApiMockMvc.perform(get(hooUrlPath)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(content().string("true"));
	}
	
	/**
	 * Test is employee currently assigned to department for thed to CMG O then false.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testIsEmployeeCurrentlyAssignedToDepartment_forThedToCMGO_thenFalse() throws Exception {
		//expect hoo is currently is assigned to CMGO
		String thedUrlPath = 
				DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
										.concat("/employees/" + DR_THED_EMPLOYEE_NO + "/isCurrentAssigned");
		
		restDepartmentApiMockMvc.perform(get(thedUrlPath)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(content().string("false"));
	}
	
	/**
	 * testIsEmployeeAssignedToDepartmentAtDate_forHongToCMGOAt2008_05_01_thenTrue
	 * http://localhost:5000/api/v1/department/{departmentCode}/employees/{employeeNo}/isAssigned/{atDate}
	 * 
	 * 1.	Hong is manager from: 2008-01-01 to 2009-12-31
	 * 2.	Hong is employee from: 2010-01-01 to present
	 *
	 * @throws Exception the exception
	 */
	
	@Test
	public void testIsEmployeeAssignedToDepartmentAtDate_forHongToCMGOAt2008_05_01_thenTrue() throws Exception {
		String hong2008_05_01UrlPath = 
				DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
										.concat("/employees/" + HONG_EMPLOYEE_NO + "/isAssigned/2008-05-01");
		
		restDepartmentApiMockMvc.perform(get(hong2008_05_01UrlPath)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(content().string("true"));
	}
	
	/**
	 * testIsEmployeeAssignedToDepartmentAtDate_forHongToCMGOAt2006_05_01_thenFalse
	 * http://localhost:5000/api/v1/department/{departmentCode}/employees/{employeeNo}/isAssigned/{atDate}
	 * 
	 * 1.	Hong is manager from: 2008-01-01 to 2009-12-31
	 * 2.	Hong is employee from: 2010-01-01 to present
	 *
	 * @throws Exception the exception
	 */
	
	@Test
	public void testIsEmployeeAssignedToDepartmentAtDate_forHongToCMGOAt2006_05_01_thenFalse() throws Exception {
		String hong2006_05_01UrlPath = 
				DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
										.concat("/employees/" + HONG_EMPLOYEE_NO + "/isAssigned/2006-05-01");
		
		restDepartmentApiMockMvc.perform(get(hong2006_05_01UrlPath)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(content().string("false"));
	}
	
	/**
	 * testIsEmployeeAssignedToDepartmentAtDate_forHongToCMGOWrongDateFormat_thenBAD_REQUEST
	 * http://localhost:5000/api/v1/department/{departmentCode}/employees/{employeeNo}/isAssigned/{atDate}
	 * 
	 * 1.	Hong is manager from: 2008-01-01 to 2009-12-31
	 * 2.	Hong is employee from: 2010-01-01 to present
	 *
	 * @throws Exception the exception
	 */
	
	@Test
	public void testIsEmployeeAssignedToDepartmentAtDate_forHongToCMGOWrongDateFormat_thenBAD_REQUEST() throws Exception {
		String hong2006_05_0122UrlPath = 
				DEPARTMENT_API_BASE_PATH.concat("/").concat(CMGO_DEPARTMENT_CODE)
										.concat("/employees/" + HONG_EMPLOYEE_NO + "/isAssigned/2006-05-0122");
		
		restDepartmentApiMockMvc.perform(get(hong2006_05_0122UrlPath)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isBadRequest());
	}
	
}
