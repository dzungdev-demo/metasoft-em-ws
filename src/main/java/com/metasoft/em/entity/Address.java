package com.metasoft.em.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Size;

import org.springframework.data.jpa.domain.AbstractPersistable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * The Class Address.
 *
 * @author Alex Dang
 */
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Address.class)
public class Address extends AbstractPersistable<Long> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6793404231743427478L;

	/** The house number. */
	@Size(max = 100)
	@Column(name = "house_number", length=100)
	private String houseNumber;
	
	/** The street. */
	@Size(max = 100)
	@Column(name = "street", length=100)
	private String street;
	
	/** The city. */
	@Size(max = 30)
	@Column(name = "city", length=30)
	private String city;
	
	/** The postcode. */
	@Size(max = 10)
	@Column(name = "postcode", length=10)
	private String postcode;

	/**
	 * Instantiates a new address.
	 */
	public Address() {
		super();
	}

	/**
	 * Instantiates a new address.
	 *
	 * @param houseNumber the house number
	 * @param street the street
	 * @param city the city
	 * @param postcode the postcode
	 */
	public Address(String houseNumber, String street, String city, String postcode) {
		super();
		this.houseNumber = houseNumber;
		this.street = street;
		this.city = city;
		this.postcode = postcode;
	}

	/**
	 * Gets the house number.
	 *
	 * @return the house number
	 */
	public String getHouseNumber() {
		return houseNumber;
	}

	/**
	 * Sets the house number.
	 *
	 * @param houseNumber the new house number
	 */
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	/**
	 * Gets the street.
	 *
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Sets the street.
	 *
	 * @param street the new street
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the postcode.
	 *
	 * @return the postcode
	 */
	public String getPostcode() {
		return postcode;
	}

	/**
	 * Sets the postcode.
	 *
	 * @param postcode the new postcode
	 */
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

}