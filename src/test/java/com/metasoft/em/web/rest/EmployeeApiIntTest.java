package com.metasoft.em.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.metasoft.em.AbstractIntegrationTest;
import com.metasoft.em.constant.Constants;
import com.metasoft.em.entity.Department;
import com.metasoft.em.entity.Employee;
import com.metasoft.em.entity.EmployeeDepartment;
import com.metasoft.em.entity.EmployeeSite;
import com.metasoft.em.entity.Site;
import com.metasoft.em.service.EmployeeService;
import com.metasoft.em.service.SiteService;
import com.metasoft.em.web.dto.EmployeeDTO;


/**
 * The Class EmployeeApiIntTest.
 *
 * @author Alex Dang
 */
public class EmployeeApiIntTest extends AbstractIntegrationTest {
	
	/** The rest employee api mock mvc. */
	private MockMvc restEmployeeApiMockMvc;
	

	/** The employee service. */
	@Autowired
	private EmployeeService employeeService;
	
	/** The site service. */
	@Autowired 
	private SiteService siteService;
	
	/** The eds. */
	private List<EmployeeDepartment> eds = new ArrayList<EmployeeDepartment>();
	
	/** The ess. */
	private List<EmployeeSite> ess = new ArrayList<EmployeeSite>();
	
	/**
	 * This method is used to initialize the Mock object for testing.
	 */
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		EmployeeApi mockEmployeeApi = new EmployeeApi();
		ReflectionTestUtils.setField(mockEmployeeApi, "employeeService", employeeService);
		
		restEmployeeApiMockMvc = MockMvcBuilders.standaloneSetup(mockEmployeeApi).build();
		
		eds.clear();
		ess.clear();
	}
	
	/**
	 * Test export csv.gi
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testExportCsv() throws Exception {
		String urlPath = EMP_API_BASE_PATH.concat("/csv");
		restEmployeeApiMockMvc.perform(get(urlPath)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM_VALUE));
	}
	
	/**
	 * Test update employee.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws Exception the exception
	 */
	@Test
	public void testUpdateEmployee() throws IOException, Exception {
		String urlPath = EMP_API_BASE_PATH + "/1";
		
		//update email from hong@gmail.com to dzungdev@gmail.com
		String dzungEmail = "dzungdev@gmail.com"; 
		Employee hongEmp = employeeService.findByEmployeeNo(HONG_EMPLOYEE_NO);
		hongEmp.setEmail(dzungEmail);
		
		restEmployeeApiMockMvc.perform(put(urlPath)
				.content(json(hongEmp))
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());
		
		hongEmp = employeeService.findOne(HONG_EMPLOYEE_ID);
		assertThat(hongEmp.getEmail()).isEqualTo(dzungEmail);
		
	}
	
	/**
	 * Test create new employee when entry date is after leaving date then CONFLICT.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws Exception the exception
	 */
	@Test 
	public void testCreateNewEmployee_whenEntryDateIsAfterLeavingDate_thenCONFLICT() throws IOException, Exception {
		
		Date date2020_01_01 = DateUtils.parseDate("2020-01-01", Constants.YYYY_MM_DD_PATTERN_ARR);
		
		Employee emp = new Employee("657435", "Wilson", "Chan", "2312312", 
				"34234", "324234", "4324234", "4234234",
				"wilsontest@gmail.com", "M", new Date(), 
				date2020_01_01, new Date(), "N", this.address);

		Department cmgoDept = departmentService.findOne(CMGO_DEPARTMENT_ID);
		
		EmployeeDepartment newEmpDept = new EmployeeDepartment(emp, cmgoDept);
		newEmpDept.setStartDate(new Date());
		
		Site stuttgart = siteService.findOne(STUTTGART_ADDRESS_ID);
		EmployeeSite newEmpSite = new EmployeeSite(emp, stuttgart);
		newEmpSite.setStartDate(new Date());
		
		eds.add(newEmpDept);
		ess.add(newEmpSite);
		
		EmployeeDTO empDTO = new EmployeeDTO();
		empDTO.setEmployee(emp);
		empDTO.setEmployeeDepartments(eds);
		empDTO.setEmployeeSites(ess);
		
		restEmployeeApiMockMvc.perform(post(EMP_API_BASE_PATH)
				.content(json(empDTO))
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isConflict());
	}
	
	/**
	 * Test create new employee when entry date is after one start date of emp site department then CONFLICT.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws Exception the exception
	 */
	@Test 
	public void testCreateNewEmployee_whenEntryDateIsAfterOneStartDateOfEmpSiteDepartment_thenCONFLICT()
		throws IOException, Exception {
		
		Date date2015_01_01 = DateUtils.parseDate("2015-01-01", Constants.YYYY_MM_DD_PATTERN_ARR);
		
		Employee emp = new Employee("657435", "Wilson", "Chan", "2312312", 
				"34234", "324234", "4324234", "4234234",
				"wilsontest@gmail.com", "M", new Date(), 
				new Date(), null, "N", this.address);

		Department cmgoDept = departmentService.findOne(CMGO_DEPARTMENT_ID);
		
		EmployeeDepartment newEmpDept = new EmployeeDepartment(emp, cmgoDept);
		newEmpDept.setStartDate(date2015_01_01);
		
		Site stuttgart = siteService.findOne(STUTTGART_ADDRESS_ID);
		EmployeeSite newEmpSite = new EmployeeSite(emp, stuttgart);
		newEmpSite.setStartDate(new Date());
		
		eds.add(newEmpDept);
		ess.add(newEmpSite);
		
		EmployeeDTO empDTO = new EmployeeDTO();
		empDTO.setEmployee(emp);
		empDTO.setEmployeeDepartments(eds);
		empDTO.setEmployeeSites(ess);
		
		restEmployeeApiMockMvc.perform(post(EMP_API_BASE_PATH)
				.content(json(empDTO))
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isConflict());
	}
	
	
}
