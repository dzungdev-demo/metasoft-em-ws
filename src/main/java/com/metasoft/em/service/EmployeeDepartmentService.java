package com.metasoft.em.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metasoft.em.constant.Constants;
import com.metasoft.em.entity.Department;
import com.metasoft.em.entity.Employee;
import com.metasoft.em.entity.EmployeeDepartment;
import com.metasoft.em.exception.ConflictException;
import com.metasoft.em.exception.DuplicatedException;
import com.metasoft.em.exception.NotFoundException;
import com.metasoft.em.repository.DepartmentRepository;
import com.metasoft.em.repository.EmployeeDepartmentRepository;
import com.metasoft.em.repository.EmployeeRepository;
import com.metasoft.em.repository.IRepository;
import com.metasoft.em.utils.DateUtil;

/**
 * The Class EmployeeDepartmentService.
 *
 * @author Alex Dang
 */
@Service
@Transactional
public class EmployeeDepartmentService extends AbstractService<EmployeeDepartment> {

	/** The employee department repository. */
	@Autowired
	private EmployeeDepartmentRepository employeeDepartmentRepository;

	/** The department repository. */
	@Autowired
	private DepartmentRepository departmentRepository;

	/** The employee repository. */
	@Autowired
	private EmployeeRepository employeeRepository;

	/**
	 * This method is provide the IRepository for based method.
	 *
	 * @return the IRepository
	 */
	@Override
	public IRepository<EmployeeDepartment> getRepository() {
		return employeeDepartmentRepository;
	}

	/**
	 * This method is used to assign one Employee to Department
	 * 
	 * We will need to check: 
	 * 1. entryDate < startDate < leaving date || leavingDate is null 
	 * 2. Checking any record of this employee in database has overlap with startDate and endDate 
	 * 3. If no overlapse, then we need to find one record of employee this have end date is null 
	 * 4. if there is one record like that, then we need to check whether the manager is change
	 * or not if it is same with new one, then don't allow to save 
	 * 5. If it is not same, then allow 	to createNew and update the endDate of previous
	 * record 
	 * 6. Check to see update or not for managerId in Department
	 *
	 * @param ed            the employee department
	 * @param isCreateEmployee the is create employee
	 * @return the employee department
	 * @throws DuplicatedException             the duplicated exception
	 * @throws ConflictException the conflict exception
	 */
	public EmployeeDepartment save(EmployeeDepartment ed, boolean isCreateEmployee) throws DuplicatedException, ConflictException {

		if (!DateUtil.isValidStartDateEndDate(ed.getStartDate(), ed.getEndDate())) {
			throw new ConflictException(msg("start.date.can.not.bigger.than.end.date"));
		}
		Employee emp = employeeRepository.findOne(ed.getEmployeeId());
		Department dept = departmentRepository.findOne(ed.getDepartmentId());
		ed.setEmployee(emp);
		ed.setDepartment(dept);
		
		validateStartEndDateWithEntryAndLeavingDate(ed, emp.getEntryDate(), emp.getLeavingDate());
		// in case we create new employee
		if (!isCreateEmployee) validateOverlapseDate(ed, true);
		updateEndDateSameEmployeeForPreviousPeriod(ed);
		// Now we can create employeeDepartmentRepository
		employeeDepartmentRepository.save(ed);
		
		updateEndDateForPreviousManager(ed);
		//After create it, we need to check to update manager_id in department
		setManagerToDepartment(ed);

		return ed;
	}
	
	/**
	 * Validate start end date with entry and leaving date.
	 *
	 * @param ed the ed
	 * @param entryDate the entry date
	 * @param leavingDate the leaving date
	 * @throws DuplicatedException the duplicated exception
	 */
	private void validateStartEndDateWithEntryAndLeavingDate(
					EmployeeDepartment ed, Date entryDate, Date leavingDate) throws DuplicatedException {
		// if ed.startDate before entryDate or ed.startDate after leavingDate
		// then we need to throw error
		if (ed.getStartDate().before(entryDate)
				|| (leavingDate != null && leavingDate.before(ed.getStartDate()))) {
			throw new DuplicatedException(msg("start.date.conflict.entry.leaving.date"));
		}
	}

	/**
	 * Update end date for previous manager.
	 *
	 * @param ed the ed
	 */
	private void updateEndDateForPreviousManager(EmployeeDepartment ed) {
		//After create it, we need to check to update manager_id in department
		//update the end date in EmployeeDepartment of previous manager
		if (ed.isManager() && 
				ed.getDepartment().getManagerId() != null) {
			EmployeeDepartment previousManagerED = 
					employeeDepartmentRepository
						.findFirstByEmployeeIdAndDepartmentIdAndManagerOrderByStartDateDesc(
								ed.getDepartment().getManagerId(), 
								ed.getDepartmentId(), Constants.BOOLEAN_YES);
			if (previousManagerED != null) {
				previousManagerED.setEndDate(DateUtils.addDays(ed.getStartDate(), -1));
				employeeDepartmentRepository.save(previousManagerED);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.metasoft.em.service.AbstractService#update(org.springframework.data.
	 * jpa.domain.AbstractPersistable)
	 */
	public EmployeeDepartment update(EmployeeDepartment ed) throws NotFoundException, 
										DuplicatedException, ConflictException {
		if (!DateUtil.isValidStartDateEndDate(ed.getStartDate(), ed.getEndDate())) {
			throw new ConflictException(msg("start.date.can.not.bigger.than.end.date"));
		}
		EmployeeDepartment existingEmpDept = getRepository().findOne(ed.getId());
		if (existingEmpDept == null) {
			throw new NotFoundException();
		}
		//check overlapse date record
		Employee emp = employeeRepository.findOne(ed.getEmployeeId());
		ed.setEmployee(emp);
		Department dept = departmentRepository.findOne(ed.getDepartmentId());
		ed.setDepartment(dept);
		
		validateStartEndDateWithEntryAndLeavingDate(ed, emp.getEntryDate(), emp.getLeavingDate());
		validateOverlapseDate(ed, false);
		
		//check if I am manager but is changed to be normal employee,
		//then update in Department
		
		
		updateEndDateSameEmployeeForPreviousPeriod(ed);
		// Now we can create employeeDepartmentRepository
		BeanUtils.copyProperties(ed, existingEmpDept);
		employeeDepartmentRepository.save(existingEmpDept);
		
		
		//After create it, we need to check to update manager_id in department
		//update the end date in EmployeeDepartment of previous manager
		updateEndDateForPreviousManager(existingEmpDept);
		setManagerToDepartment(existingEmpDept);
		return existingEmpDept;
	}

	/**
	 * Update end date same employee for previous period.
	 *
	 * @param ed the ed
	 */
	private void updateEndDateSameEmployeeForPreviousPeriod(EmployeeDepartment ed) {
		// no overlapsed, so we find any record which has endDate is null to update
		List<EmployeeDepartment> sameEmpForDeptButEndDateNullEDs = 
				employeeDepartmentRepository.findSameEmployeeDepartmentAndEndDateNull(
												ed.getEmployeeId(), ed.getDepartmentId(), ed.getStartDate());
		// update end date of previous button to be the date 
		if (!sameEmpForDeptButEndDateNullEDs.isEmpty()) {
			EmployeeDepartment sameEmpForDeptButEndDateNullED = sameEmpForDeptButEndDateNullEDs.get(0);
			sameEmpForDeptButEndDateNullED.setEndDate(DateUtils.addDays(ed.getStartDate(), -1));
			employeeDepartmentRepository.save(sameEmpForDeptButEndDateNullED);
		}
	}
	
	/**
	 * Validate overlapse date.
	 *
	 * @param ed the ed
	 * @param isCreate the is create
	 * @throws DuplicatedException the duplicated exception
	 */
	public void validateOverlapseDate(EmployeeDepartment ed, boolean isCreate) throws DuplicatedException {
		//checking overlapsed
		List<EmployeeDepartment> overlapsedEDs = new ArrayList<EmployeeDepartment>();
		if (isCreate) {
			overlapsedEDs = employeeDepartmentRepository.findOverlapsedEmpDept(
															ed.getEmployeeId(), 
															ed.getDepartmentId(), 
															ed.getStartDate(),
															ed.getEndDate());
		} else {
			overlapsedEDs = employeeDepartmentRepository.findOverlapsedEmpDeptExceptId(
															ed.getEmployeeId(), 
															ed.getDepartmentId(), 
															ed.getStartDate(),
															ed.getEndDate(),
															ed.getId());
		}				
		if (!overlapsedEDs.isEmpty()) {
			throw new DuplicatedException(msg("employee.edit.department.site.overlapsed"));
		}
	}
	
	/**
	 * Delete in batch.
	 *
	 * @param employeeDepartments
	 *            the employee departments
	 * @throws NotFoundException
	 *             the not found exception
	 */
	public void deleteInBatch(List<EmployeeDepartment> employeeDepartments) throws NotFoundException {
		deleteDepartmentManagerInBatch(employeeDepartments);
		employeeDepartmentRepository.deleteInBatch(employeeDepartments);
	}

	/**
	 * Find by employee id.
	 *
	 * @param employeeId
	 *            the employee id
	 * @return the list
	 */
	public List<EmployeeDepartment> findByEmployeeId(Long employeeId) {
		return employeeDepartmentRepository.findByEmployeeId(employeeId);
	}

	/**
	 * Delete by employee id and department id.
	 *
	 * @param employeeId
	 *            the employee id
	 * @param departmentId
	 *            the department id
	 * @return the long
	 */
	public Long deleteByEmployeeIdAndDepartmentId(Long employeeId, Long departmentId) {
		return employeeDepartmentRepository.deleteByEmployeeIdAndDepartmentId(employeeId, departmentId);
	}

	/**
	 * Before we delete employeeDepartments, we need to make sure if user is
	 * manager of department, we will need to remove manager from department.
	 *
	 * @param employeeDepartments
	 *            the employee departments
	 * @throws NotFoundException
	 *             the not found exception
	 */
	public void deleteDepartmentManagerInBatch(List<EmployeeDepartment> employeeDepartments) throws NotFoundException {
		employeeDepartments.forEach(ed -> {
			if (ed.isManager()) {
				ed.getDepartment().setManagerId(null);
				departmentRepository.save(ed.getDepartment());
			}
		});
	}

	/**
	 * Sets the manager to department.
	 *
	 * @param employeeDepartments
	 *            the new manager to department
	 */
	public void setManagerToDepartment(List<EmployeeDepartment> employeeDepartments) {
		employeeDepartments.forEach(ed -> {
			setManagerToDepartment(ed);
		});
	}

	/**
	 * Sets the manager to department.
	 *
	 * @param empDept the new manager to department
	 */
	private void setManagerToDepartment(EmployeeDepartment empDept) {
		if (empDept.isManager()) {
			empDept.getDepartment().setManagerId(empDept.getEmployeeId());
			empDept.getDepartment().setManagerName(empDept.getEmployee().getFullName());
			departmentRepository.save(empDept.getDepartment());
		}
	}
	
	/**
	 * Checks if is overlapsed between StartDate and EndDate of 
	 * EmployeeDepartment List.
	 *
	 * @param eds the eds
	 * @return true, if is overlapsed
	 */
	public boolean isOverlapsed(List<EmployeeDepartment> eds) {
		if (eds.size() < 2) return false;
		
		Map<Long, List<EmployeeDepartment>> edsMap = 
										eds.stream()
										   .collect(
												Collectors.groupingBy(EmployeeDepartment::getDepartmentId));
		for (Long deptId: edsMap.keySet()) {
			List<EmployeeDepartment> empDepts = edsMap.get(deptId);
			empDepts.stream()
			   .sorted((ed1, ed2) -> ed1.getStartDate().compareTo(ed2.getStartDate()));
			for (int i = 0; i <= eds.size() - 2; i++) {
				EmployeeDepartment ed1 = empDepts.get(i);
				EmployeeDepartment ed2 = empDepts.get(i+1);
				if (ed1.getEndDate() == null ||
						ed1.getEndDate().after(ed2.getStartDate())) { 
							return true;
				}			
			}
		}
		return false;
	}

}
