package com.metasoft.em.web.rest;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metasoft.em.entity.Department;
import com.metasoft.em.entity.Employee;
import com.metasoft.em.exception.DuplicatedException;
import com.metasoft.em.exception.NotFoundException;
import com.metasoft.em.service.DepartmentService;
import com.metasoft.em.service.EmployeeService;

/**
 * The Class DepartmentApi.
 *
 * @author Alex Dang
 */
@RestController
@RequestMapping(value = "/api/v1/departments")
public class DepartmentApi {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(DepartmentApi.class);

	/** The department service. */
	@Autowired
	private DepartmentService departmentService;

	/** The employee service. */
	@Autowired
	private EmployeeService employeeService;

	/**
	 * Find all.
	 *
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Department>> findAll() {
		log.info("start findAll department");
		List<Department> departments = departmentService.findAll();
		if (departments.isEmpty()) {
			log.info("end findAll department NO_CONTENT:204");
			return new ResponseEntity<List<Department>>(HttpStatus.NO_CONTENT);
		}
		log.info("end findAll department OK:200");
		return new ResponseEntity<>(departments, HttpStatus.OK);
	}

	/**
	 * Find by id.
	 *
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Department> findById(@PathVariable("id") long id) {
		log.info("start findById with id is {}", id);
		
		Department department = departmentService.findOne(id);
		if (department == null) {
			return new ResponseEntity<Department>(HttpStatus.NO_CONTENT);
		}
		log.info("end findById with id is {}", id);
		return new ResponseEntity<Department>(department, HttpStatus.OK);
	}

	/**
	 * Creates a Department.
	 *
	 * @param department
	 *            the department
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> create(@Validated @RequestBody Department department) {
		log.info("start creating new department with department code {}", department.getDepartmentCode());
		
		try {
			departmentService.save(department);
			log.info("end creating new department with department code {}", department.getDepartmentCode());
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		} catch (DuplicatedException de) {
			log.error("conflict creating new department with department code {}", department.getDepartmentCode());
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}

	/**
	 * Update department.
	 *
	 * @param department            the Department
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@Validated @RequestBody Department department) {
		log.info("start update department with department code {}", department.getDepartmentCode());
		try {
			departmentService.update(department);
			log.info("end update department with department code {}", department.getDepartmentCode());
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (NotFoundException nfe) {
			log.error("Notfound: 404 in update department with department code {}", department.getDepartmentCode());
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} catch (DuplicatedException de) {
			log.error("Notfound: 409 in update department with department code {}", department.getDepartmentCode());
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}

	/**
	 * Delete Department.
	 *
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Department> delete(@PathVariable("id") long id) {
		log.info("start deleting new department with department id {}", id);
		
		try {
			departmentService.delete(id);
		} catch (NotFoundException nfe) {
			log.error("Not found in deleting department with department id {}", id);
			
			return new ResponseEntity<Department>(HttpStatus.NOT_FOUND);
		} catch (DataIntegrityViolationException dive) {
			log.error("CONFLICT in deleting department with department id {}", id);
			
			return new ResponseEntity<Department>(HttpStatus.CONFLICT);
		}
		log.info("end deleting department with department id {}", id);
		
		return new ResponseEntity<Department>(HttpStatus.OK);

	}

	/**
	 * Find current manager by department code.
	 *
	 * @param departmentCode
	 *            the department code
	 * @return the response entity
	 */
	@RequestMapping(value = "/{departmentCode}/employees/current/manager", method = RequestMethod.GET)
	public ResponseEntity<Employee> findCurrentManagerByDepartmentCode(
			@PathVariable("departmentCode") String departmentCode) {
		log.info("start findCurrentManagerByDepartmentCode: with departmentCode {}", departmentCode);
		
		Employee employee = employeeService.findCurrentManagerByDepartmentCode(departmentCode);
		if (employee == null) {
			log.info("end findCurrentManagerByDepartmentCode: NO_CONTENT:204 with departmentCode {}", departmentCode);
			
			return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
		} else {
			log.info("end findCurrentManagerByDepartmentCode: OK:200 with departmentCode {}", departmentCode);
			
			return new ResponseEntity<Employee>(employee, HttpStatus.OK);
		}
	}

	/**
	 * Find manager by department code at date.
	 *
	 * @param departmentCode
	 *            the department code
	 * @param atDate
	 *            the at date
	 * @return the response entity
	 */
	@RequestMapping(value = "/{departmentCode}/employees/manager/{atDate}", method = RequestMethod.GET)
	public ResponseEntity<Employee> findManagerByDepartmentCodeAtDate(
			@PathVariable("departmentCode") String departmentCode,
			@PathVariable("atDate") @DateTimeFormat(iso = ISO.DATE) Date atDate) {
		log.info("start findManagerByDepartmentCodeAtDate: with departmentCode {} and date {}", departmentCode,
				atDate);
		Employee manager = employeeService.findManagerByDepartmentCodeAtDate(departmentCode, atDate);
		if (manager == null) {
			log.info("end findManagerByDepartmentCodeAtDate: NO_CONTENT:204 with departmentCode {} and date {}",
					departmentCode, atDate);
			return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
		} else {
			log.info("end findManagerByDepartmentCodeAtDate: OK:200 with departmentCode {} and date {}",
					departmentCode, atDate);
			return new ResponseEntity<Employee>(manager, HttpStatus.OK);
		}
	}

	/**
	 * Find current employees by department code.
	 *
	 * @param departmentCode
	 *            the department code
	 * @return the response entity
	 */
	@RequestMapping(value = "/{departmentCode}/employees/current", method = RequestMethod.GET)
	public ResponseEntity<List<Employee>> findCurrentEmployeesByDepartmentCode(
			@PathVariable("departmentCode") String departmentCode) {
		log.info("start findCurrentEmployeesByDepartmentCode: with departmentCode {}", departmentCode);
		List<Employee> employee = employeeService.findCurrentEmployeesByDepartmentCode(departmentCode);
		if (employee.isEmpty()) {
			log.info("end findCurrentEmployeesByDepartmentCode: NO_CONTENT:204 with departmentCode {}",
					departmentCode);
			return new ResponseEntity<List<Employee>>(HttpStatus.NO_CONTENT);
		} else {
			log.info("end findCurrentEmployeesByDepartmentCode: OK:200 with departmentCode {}", departmentCode);
			return new ResponseEntity<List<Employee>>(employee, HttpStatus.OK);
		}
	}

	/**
	 * Find employees by department code at date.
	 *
	 * @param departmentCode
	 *            the department code
	 * @param atDate
	 *            the at date
	 * @return the response entity
	 */
	@RequestMapping(value = "/{departmentCode}/employees/{atDate}", method = RequestMethod.GET)
	public ResponseEntity<List<Employee>> findEmployeesByDepartmentCodeAtDate(
			@PathVariable("departmentCode") String departmentCode,
			@PathVariable("atDate") @DateTimeFormat(iso = ISO.DATE) Date atDate) {
		log.info("start findEmployeesByDepartmentCodeAtDate: with departmentCode {} at date {}", departmentCode,
				atDate);
		List<Employee> employees = employeeService.findEmployeesByDepartmentCodeAtDate(departmentCode, atDate);
		if (employees.isEmpty()) {
			log.info("end findEmployeesByDepartmentCodeAtDate: NO_CONTENT:204 with departmentCode {} at date {}",
					departmentCode, atDate);
			return new ResponseEntity<List<Employee>>(HttpStatus.NO_CONTENT);
		} else {
			log.info("end findEmployeesByDepartmentCodeAtDate: OK:200 with departmentCode {} at date {}",
					departmentCode, atDate);
			return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
		}
	}

	/**
	 * Checks if is employee currently assigned to department.
	 *
	 * @param departmentCode
	 *            the department code
	 * @param employeeNo
	 *            the employee no
	 * @return the response entity
	 */
	@RequestMapping(value = "/{departmentCode}/employees/{employeeNo}/isCurrentAssigned", method = RequestMethod.GET)
	public ResponseEntity<Boolean> isEmployeeCurrentlyAssignedToDepartment(
			@PathVariable("departmentCode") String departmentCode, @PathVariable("employeeNo") String employeeNo) {
		log.info("start isEmployeeCurrentlyAssignedToDepartment: with departmentCode {} and employeeNO {}",
						departmentCode, employeeNo);
		boolean isCurrentlyAssigned = employeeService.isEmployeeCurrentlyAssignedToDepartment(employeeNo,
				departmentCode);
		log.info("end isEmployeeCurrentlyAssignedToDepartment: with departmentCode {} and employeeNO {}",
				departmentCode, employeeNo);
		return new ResponseEntity<Boolean>(isCurrentlyAssigned, HttpStatus.OK);
	}

	/**
	 * Checks if is employee assigned to department at date.
	 *
	 * @param departmentCode
	 *            the department code
	 * @param employeeNo
	 *            the employee no
	 * @param atDate
	 *            the at date
	 * @return the response entity
	 */
	@RequestMapping(value = "/{departmentCode}/employees/{employeeNo}/isAssigned/{atDate}", method = RequestMethod.GET)
	public ResponseEntity<Boolean> isEmployeeAssignedToDepartmentAtDate(
											@PathVariable("departmentCode") String departmentCode, 
											@PathVariable("employeeNo") String employeeNo,
											@PathVariable("atDate") @DateTimeFormat(iso = ISO.DATE) Date atDate) {
		log.info("start isEmployeeAssignedToDepartmentAtDate: with departmentCode {} , employeeNo {} atDate {}",
				departmentCode,employeeNo, atDate);
		boolean isAssigned = employeeService.isEmployeeAssignedToDepartmentAtDate(employeeNo, departmentCode, atDate);
		log.info("end isEmployeeAssignedToDepartmentAtDate: with departmentCode {} , employeeNo {} atDate {}",
					departmentCode,employeeNo, atDate);
		
		return new ResponseEntity<Boolean>(isAssigned, HttpStatus.OK);
	}

}
