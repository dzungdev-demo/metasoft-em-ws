package com.metasoft.em.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.metasoft.em.AbstractIntegrationTest;
import com.metasoft.em.entity.Address;
import com.metasoft.em.entity.Site;
import com.metasoft.em.service.EmployeeService;
import com.metasoft.em.service.SiteService;

/**
 * The Class SiteApiIntTest.
 *
 * @author Alex Dang
 */
public class SiteApiIntTest extends AbstractIntegrationTest {
	/** The department service. */
	@Autowired
	private SiteService siteService;
	
	/** The employee service. */
	@Autowired
	private EmployeeService employeeService;
	
	/** The rest department api mock mvc. */
	private MockMvc restSiteApiMockMvc;
	
	/**
	 * This method is used to prepare MockMvc to test Restful web service.
	 */
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		SiteApi mockSiteApi = new SiteApi();
		ReflectionTestUtils.setField(mockSiteApi, "siteService", siteService);
		ReflectionTestUtils.setField(mockSiteApi, "employeeService", employeeService);
		
		restSiteApiMockMvc = MockMvcBuilders.standaloneSetup(mockSiteApi).build();
	}
	
	/**
	 * test api for findCurrentEmployeesBySiteName
	 * http://localhost:5000/api/v1/sites/{siteName}/employees/current
	 * 
	 * 2 Employees Hoo, Dr Thed is currently assigned to site: stuttgart
	 * expect: 3 employees.
	 *
	 * @throws Exception the exception
	 */
	
	@Test
	public void testFindCurrentEmployeesBySiteName() throws Exception {
		String currentEmployeesUrlPath = 
				SITE_API_BASE_PATH.concat("/").concat(STUTTGART_SITE_NAME)
										.concat("/employees/current");
		
		restSiteApiMockMvc.perform(get(currentEmployeesUrlPath)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$[0].employeeNo").value(HOO_EMPLOYEE_NO))
				.andExpect(jsonPath("$[1].employeeNo").value(DR_THED_EMPLOYEE_NO));
	}
	
	
	
	/**
	 * test FindEmployeesBySiteNameAtDate_forStuttgart_2011_01_01_returnHong_Thed
	 * http://localhost:5000/api/v1/sites/{siteName}/employees/{atDate}
	 * 
	 * 1. Hong is assigned to Stuttgart from 2010-05-10 to 2015-10-10
	 * 2. Hoo is assigned to Stuttgart from 2015-05-10 to present
	 * 3. Thed is assigned to Stuttgart from 2010-05-10 to present
	 * 4. Chew is assigned to Essen from 2015-02-03 to present
	 * 5. Wilson is assigned to Hanover from 2016-01-01 to present
	 * 
	 * @throws Exception the exception
	 */
	
	@Test
	public void testFindEmployeesBySiteNameAtDate_forStuttgart_2011_01_01_returnHong_Thed() throws Exception {
		String stuttgart2011_01_01UrlPath = 
				SITE_API_BASE_PATH.concat("/").concat(STUTTGART_SITE_NAME)
										.concat("/employees/2011-01-01");
		
		restSiteApiMockMvc.perform(get(stuttgart2011_01_01UrlPath)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].employeeNo").value(HONG_EMPLOYEE_NO))
				.andExpect(jsonPath("$[1].employeeNo").value(DR_THED_EMPLOYEE_NO));
	}
	
	/**
	 * test api for findEmployeesBySiteNameAtDate
	 * http://localhost:5000/api/v1/sites/{siteName}/employees/{atDate}
	 * 
	 * 1. Hong is assigned to Stuttgart from 2010-05-10 to 2015-10-10
	 * 2. Hoo is assigned to Stuttgart from 2015-05-10 to present
	 * 3. Thed is assigned to Stuttgart from 2010-05-10 to present
	 * 4. Chew is assigned to Essen from 2015-02-03 to present
	 * 5. Wilson is assigned to Hanover from 2016-01-01 to present
	 * 
	 * @throws Exception the exception
	 */
	
	@Test
	public void testFindEmployeesBySiteNameAtDate_forStuttgart_2016_08_01_returnHoo_Thed() throws Exception {
		String stuttgart2016_08_01UrlPath = 
				SITE_API_BASE_PATH.concat("/").concat(STUTTGART_SITE_NAME)
										.concat("/employees/2016-08-01");
		
		restSiteApiMockMvc.perform(get(stuttgart2016_08_01UrlPath)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].employeeNo").value(HOO_EMPLOYEE_NO))
				.andExpect(jsonPath("$[1].employeeNo").value(DR_THED_EMPLOYEE_NO));
	}
	
	/**
	 * test FindEmployeesBySiteNameAtDate_forStuttgartWrongDateFormat_returnBAD_REQUEST
	 * http://localhost:5000/api/v1/sites/{siteName}/employees/{atDate}
	 * 
	 * 1. Hong is assigned to Stuttgart from 2010-05-10 to 2015-10-10
	 * 2. Hoo is assigned to Stuttgart from 2015-05-10 to present
	 * 3. Thed is assigned to Stuttgart from 2010-05-10 to present
	 * 4. Chew is assigned to Essen from 2015-02-03 to present
	 * 5. Wilson is assigned to Hanover from 2016-01-01 to present
	 * 
	 * @throws Exception the exception
	 */
	
	@Test
	public void testFindEmployeesBySiteNameAtDate_forStuttgartWrongDateFormat_returnBAD_REQUEST() throws Exception {
		String stuttgart2016_08_0111UrlPath = 
				SITE_API_BASE_PATH.concat("/").concat(STUTTGART_SITE_NAME)
										.concat("/employees/2011-01-1111");
		
		restSiteApiMockMvc.perform(get(stuttgart2016_08_0111UrlPath)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isBadRequest());
	}
	
	/**
	 * Test delete new site which does not have any employee then successfully.
	 *
	 * @throws Exception the exception
	 */
	@Test 
	public void testDeleteNewSiteWhichDoesNotHaveAnyEmployee_thenSuccessfully() throws Exception {
		Site frankfurtSite = new Site("Frankfurt", 112,3345, "Y",
										new Address("40A", "Tree st", "frankfurt", "111"));
		siteService.save(frankfurtSite);
		
		Long frankfurtId = frankfurtSite.getId();
		String frankfurtDeletingURL = 
				SITE_API_BASE_PATH.concat("/").concat(frankfurtId.toString());
		restSiteApiMockMvc.perform(delete(frankfurtDeletingURL)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isOk());
		
		frankfurtSite = siteService.findOne(frankfurtId);
		assertThat(frankfurtSite).isNull();
	}
}