package com.metasoft.em.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metasoft.em.entity.EmployeeSite;
import com.metasoft.em.exception.ConflictException;
import com.metasoft.em.exception.DuplicatedException;
import com.metasoft.em.exception.NotFoundException;
import com.metasoft.em.service.EmployeeSiteService;
import com.metasoft.em.web.dto.error.ErrorDTO;

/**
 * The Class EmployeeSiteApi.
 *
 * @author Alex Dang
 */
@RestController
@RequestMapping(value = "/api/v1/employeeSites")
public class EmployeeSiteApi {
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(EmployeeSiteApi.class);

	/** The employee site service. */
	@Autowired
	private EmployeeSiteService employeeSiteService;

	/**
	 * Find by id.
	 *
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<EmployeeSite> findById(@PathVariable("id") long id) {
		log.debug("start findById with employeeSite id {} ", id);
		EmployeeSite employeeSite = employeeSiteService.findOne(id);
		if (employeeSite == null) {
			log.debug("end findById: NO_CONTENT:204 with employeeSite id {} ", id);
			return new ResponseEntity<EmployeeSite>(HttpStatus.NO_CONTENT);
		}
		log.debug("end findById: OK:204 with employeeSite id {} ", id);
		return new ResponseEntity<EmployeeSite>(employeeSite, HttpStatus.OK);
	}

	/**
	 * Creates the employee site.
	 *
	 * @param employeeSite the employee site
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> createEmployeeSite(@Validated @RequestBody EmployeeSite employeeSite) {
		log.debug("start createEmployeeSite with employee id {} and site id ", 
						employeeSite.getEmployeeId(), employeeSite.getSiteId());
		try {
			employeeSiteService.save(employeeSite);
			log.debug("end createEmployeeSite: CREATED:201 with employee id {} and site id ", 
							employeeSite.getEmployeeId(), employeeSite.getSiteId());
			
			return new ResponseEntity<Object>(employeeSite, HttpStatus.CREATED);
		} catch (DuplicatedException | ConflictException ex) {
			log.debug("end createEmployeeSite: CONFLICT:409 with employee id {} and site id ", 
					employeeSite.getEmployeeId(), employeeSite.getSiteId());
			
			return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorDTO(ex.getMessage(), HttpStatus.CONFLICT.value()));
		}
	}

	/**
	 * Update employee site.
	 *
	 * @param id the id
	 * @param employeeSite the employee site
	 * @return the response entity
	 */
	@RequestMapping(value="/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateEmployeeSite(@PathVariable Long id, 
										@Validated @RequestBody EmployeeSite employeeSite) {
		log.debug("start updateEmployeeSite with employeeSiteId {}", employeeSite.getId());
		try {
			employeeSiteService.update(employeeSite);
			log.debug("end updateEmployeeSite: OK:200 with employeeSiteId {}", employeeSite.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} catch (NotFoundException nfe) {
			log.debug("end updateEmployeeSite: NOT_FOUND:404 with employeeSiteId {}", employeeSite.getId());
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorDTO(nfe.getMessage(), HttpStatus.NOT_FOUND.value()));
		} catch (DuplicatedException | ConflictException de) {
			log.debug("end updateEmployeeSite: NOT_FOUND:409 with employeeSiteId {}", employeeSite.getId());
			return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorDTO(de.getMessage(), HttpStatus.CONFLICT.value()));
		}
	}

	/**
	 * Delete employeeSite.
	 *
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<EmployeeSite> delete(@PathVariable("id") long id) {
		log.debug("start deleteEmployeeSite with employeeSiteId {}", id);
		try {
			employeeSiteService.delete(id);
		} catch (NotFoundException nfe) {
			log.debug("start deleteEmployeeSite: NOT_FOUND:404 with employeeSiteId {}", id);
			return new ResponseEntity<EmployeeSite>(HttpStatus.NOT_FOUND);
		}
		log.debug("start deleteEmployeeSite: OK:200 with employeeSiteId {}", id);
		return new ResponseEntity<EmployeeSite>(HttpStatus.OK);
	}

}
