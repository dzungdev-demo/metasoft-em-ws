package com.metasoft.em.service;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metasoft.em.entity.Site;
import com.metasoft.em.exception.DuplicatedException;
import com.metasoft.em.exception.NotFoundException;
import com.metasoft.em.repository.IRepository;
import com.metasoft.em.repository.SiteRepository;

/**
 * The Class SiteService.
 *
 * @author Alex Dang
 */
@Service
@Transactional
public class SiteService extends AbstractService<Site>{
	
	/** The site repository. */
	@Autowired
	private SiteRepository siteRepository;

	/**
	 * This method is provide the IRepository for based method.
	 *
	 * @return the IRepository
	 */
	@Override
	public IRepository<Site> getRepository() {
		return siteRepository;
	}
	
	/**
	 * Save.
	 *
	 * @param site the site
	 * @return the site
	 * @throws DuplicatedException the duplicated exception
	 */
	/* (non-Javadoc)
	 * @see com.metasoft.em.service.AbstractService#save(org.springframework.data.jpa.domain.AbstractPersistable)
	 */
	public Site save(Site site) throws DuplicatedException {
		Site existingSite = 
					siteRepository.findBySiteName(site.getSiteName());
		if (existingSite != null) {
			throw new DuplicatedException(msg("site.duplicate.sitename", site.getSiteName()));
		}
		getRepository().save(site);
		return site;
	}
	
	/**
	 * Update.
	 *
	 * @param site the Site
	 * @return the Site
	 * @throws NotFoundException the not found exception
	 * @throws DuplicatedException the duplicated exception
	 */
	public Site update(Site site) throws NotFoundException, DuplicatedException {
		Site existingSite = getRepository().findOne(site.getId());
		if (existingSite == null) {
			throw new NotFoundException();
		}
		if (!site.getSiteName().equals(existingSite.getSiteName()) &&
				siteRepository.findBySiteName(site.getSiteName()) != null) {
			throw new DuplicatedException(msg("site.duplicate.sitename", site.getSiteName()));
		}
		
		BeanUtils.copyProperties(site, existingSite);
		getRepository().save(existingSite);
		return existingSite;
	}

}
