package com.metasoft.em.web.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.metasoft.em.entity.Employee;
import com.metasoft.em.entity.EmployeeDepartment;
import com.metasoft.em.entity.EmployeeSite;

/**
 * The Class EmployeeDTO.
 *
 * @author Alex Dang
 */
public class EmployeeDTO {
	
	/** The employee. */
	@NotNull
	private Employee employee;
	
	/** The employee departments. */
	@NotEmpty
	private List<EmployeeDepartment> employeeDepartments;
	
	/** The employee sites. */
	@NotEmpty
	private List<EmployeeSite> employeeSites;

	/**
	 * Gets the employee.
	 *
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * Sets the employee.
	 *
	 * @param employee the new employee
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * Gets the employee departments.
	 *
	 * @return the employee departments
	 */
	public List<EmployeeDepartment> getEmployeeDepartments() {
		return employeeDepartments;
	}

	/**
	 * Sets the employee departments.
	 *
	 * @param employeeDepartments the new employee departments
	 */
	public void setEmployeeDepartments(List<EmployeeDepartment> employeeDepartments) {
		this.employeeDepartments = employeeDepartments;
	}

	/**
	 * Gets the employee sites.
	 *
	 * @return the employee sites
	 */
	public List<EmployeeSite> getEmployeeSites() {
		return employeeSites;
	}

	/**
	 * Sets the employee sites.
	 *
	 * @param employeeSites the new employee sites
	 */
	public void setEmployeeSites(List<EmployeeSite> employeeSites) {
		this.employeeSites = employeeSites;
	}
	
}
