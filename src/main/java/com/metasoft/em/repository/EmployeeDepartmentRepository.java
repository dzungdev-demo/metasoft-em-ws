package com.metasoft.em.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.metasoft.em.entity.EmployeeDepartment;

/**
 * The Interface EmployeeDepartmentRepository.
 *
 * @author Alex Dang
 */
public interface EmployeeDepartmentRepository extends IRepository<EmployeeDepartment> {
	
	/**
	 * Find by employee id and department id.
	 *
	 * @param employeeId the employee id
	 * @param departmentId the department id
	 * @return the employee department
	 */
	public EmployeeDepartment findByEmployeeIdAndDepartmentId(Long employeeId, Long departmentId);
	
	/**
	 * Find by employee id.
	 *
	 * @param employeeId the employee id
	 * @return the list
	 */
	public List<EmployeeDepartment> findByEmployeeId(Long employeeId);
	
	/**
	 * Delete by employee id and department id.
	 *
	 * @param employeeId the employee id
	 * @param departmentId the department id
	 * @return the long
	 */
	public Long deleteByEmployeeIdAndDepartmentId(Long employeeId, Long departmentId);
	
	/**
	 * Find first by department id and manager order by id desc.
	 *
	 * @param departmentId the department id
	 * @param manager the manager
	 * @return the employee department
	 */
	public EmployeeDepartment findFirstByDepartmentIdAndManagerOrderByIdDesc(Long departmentId, String manager);
	
	/**
	 * Find overlapsed EmployeeDepartment
	 * 			SD1<=SD2<=ED1 OR SD1<=ED2<=ED1 OR SD2<=SD1<=ED2 OR SD2<=ED1<=ED2.
	 *
	 * @param employeeId the employee id
	 * @param departmentId the department id
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return the list
	 */
	@Query("select ed from EmployeeDepartment ed where"
			+ " ed.departmentId = :departmentId and ed.employeeId = :employeeId "
			+ " and ("
			+ "		(ed.startDate between :startDate and :endDate) or"
			+ "     (ed.endDate between :startDate and :endDate) or"
			+ "		(:startDate between ed.startDate and ed.endDate) or"
			+ "		(:endDate between ed.startDate and ed.endDate)"
			+ " ) ")
	public List<EmployeeDepartment> findOverlapsedEmpDept(
											@Param("employeeId") Long employeeId,
											@Param("departmentId") Long departmentId, 
											@Param("startDate") Date startDate, 
											@Param("endDate") Date endDate);
	
	
	
	/**
	 * Find overlapsed emp dept except id.
	 *
	 * @param employeeId the employee id
	 * @param departmentId the department id
	 * @param startDate the start date
	 * @param endDate the end date
	 * @param id the id
	 * @return the list
	 */
	@Query("select ed from EmployeeDepartment ed where"
			+ " ed.departmentId = :departmentId and ed.employeeId = :employeeId "
			+ "	and ed.id <> :id "
			+ " and ("
			+ "		(ed.startDate between :startDate and :endDate) or"
			+ "     (ed.endDate between :startDate and :endDate) or"
			+ "		(:startDate between ed.startDate and ed.endDate) or"
			+ "		(:endDate between ed.startDate and ed.endDate)"
			+ " ) ")
	public List<EmployeeDepartment> findOverlapsedEmpDeptExceptId(
											@Param("employeeId") Long employeeId,
											@Param("departmentId") Long departmentId, 
											@Param("startDate") Date startDate, 
											@Param("endDate") Date endDate,
											@Param("id") Long id);
	
	
	/**
	 * Finding list of employeeDepartment which have startDate is 
	 * before the new StartDate and endDate of prev ED is null.
	 *
	 * @param employeeId the employee id
	 * @param departmentId the department id
	 * @param startDate the start date
	 * @return List<EmployeeDepartment>
	 */
	
	@Query("select ed from EmployeeDepartment ed where"
			+ " ed.departmentId = :departmentId and ed.employeeId = :employeeId " 
			+ " and ed.startDate < :startDate"
			+ " order by ed.startDate desc")
	public List<EmployeeDepartment> findSameEmployeeDepartmentAndEndDateNull(
											@Param("employeeId") Long employeeId,
											@Param("departmentId") Long departmentId ,
											@Param("startDate") Date startDate);
	
	
	/**
	 * Find first by employment id and department id and manager order by start date desc.
	 *
	 * @param empId the emp id
	 * @param deptId the dept id
	 * @param manager the manager
	 * @return the EmployeeDepartment
	 */
	public EmployeeDepartment 
				findFirstByEmployeeIdAndDepartmentIdAndManagerOrderByStartDateDesc(
						Long empId, Long deptId, String manager);
	
}
