package com.metasoft.em.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.metasoft.em.AbstractIntegrationTest;
import com.metasoft.em.constant.Constants;
import com.metasoft.em.entity.Department;
import com.metasoft.em.entity.Employee;
import com.metasoft.em.entity.EmployeeDepartment;
import com.metasoft.em.entity.EmployeeSite;
import com.metasoft.em.entity.Site;
import com.metasoft.em.exception.ConflictException;
import com.metasoft.em.exception.DuplicatedException;
import com.metasoft.em.exception.NotFoundException;
import com.metasoft.em.repository.SiteRepository;

/**
 * The Class EmployeeServiceIntTest.
 *
 * @author Alex Dang
 */
public class EmployeeServiceIntTest extends AbstractIntegrationTest {

	/** The employee service. */
	@Autowired
	private EmployeeService employeeService;
	
	/** The site service. */
	@Autowired 
	private SiteService siteService;
	
	/** The site repository. */
	@Autowired
	private SiteRepository siteRepository;
	
	/** The eds. */
	private List<EmployeeDepartment> eds = new ArrayList<EmployeeDepartment>();
	
	/** The ess. */
	private List<EmployeeSite> ess = new ArrayList<EmployeeSite>();
	
	/** The date 2030 12 31. */
	Date date2030_12_31 = null;
	
	/**
	 * Inits the.
	 *
	 * @throws ParseException the parse exception
	 */
	@Before
	public void init() throws ParseException {
		eds.clear();
		ess.clear();
		date2030_12_31 = DateUtils.parseDate("2030-12-31", Constants.YYYY_MM_DD_PATTERN_ARR);
	}

	/**
	 * When we run with initial data, we always have 5 employees, so when we
	 * create new employee, the latest employeeNo should be EMP000006.
	 *
	 * @throws DuplicatedException             the duplicated exception
	 * @throws ParseException             the parse exception
	 * @throws ConflictException the conflict exception
	 */
	@Test
	public void testCreatingNewEmployeeHavingLatestEmployeeNo() throws 
					DuplicatedException, ParseException, ConflictException {

		Employee emp = new Employee("657435", "Wilson", "Chan", "2312312", 
									"34234", "324234", "4324234", "4234234",
									"wilsontest@gmail.com", "M", new Date(), new Date(), 
									date2030_12_31, "N", this.address);
		
		EmployeeDepartment ed = new EmployeeDepartment(emp, cmgoDepartment);
		ed.setStartDate(new Date());
		ed.setEndDate(date2030_12_31);
		eds.add(ed);
		
		Site essenSite = siteRepository.findBySiteName(ESSEN_SITE_NAME);
		EmployeeSite es = new EmployeeSite(emp, essenSite);
		es.setStartDate(new Date());
		es.setEndDate(date2030_12_31);
		ess.add(es);
		
		employeeService.save(emp, eds, ess);

		assertThat(emp.getEmployeeNo()).isEqualTo("EMP00006");
		assertThat(employeeService.count()).isEqualTo(6);
	}
	
	/**
	 * Test creating new employee.
	 *
	 * @throws DuplicatedException the duplicated exception
	 * @throws ParseException the parse exception
	 * @throws ConflictException the conflict exception
	 */
	@Test
	public void testCreatingNewEmployee() 
				throws DuplicatedException, ParseException, ConflictException {
		Employee emp = new Employee("657435", "Wilson", "Chan", "2312312", 
									"34234", "324234", "4324234", "4234234",
									"wilsontest@gmail.com", "M", new Date(), 
									new Date(), date2030_12_31, "N", this.address);
		
		Department cmgoDept = departmentService.findOne(CMGO_DEPARTMENT_ID);
		
		EmployeeDepartment newEmpDept = new EmployeeDepartment(emp, cmgoDept);
		newEmpDept.setStartDate(new Date());
		newEmpDept.setEndDate(date2030_12_31);
		
		Site stuttgart = siteService.findOne(STUTTGART_ADDRESS_ID);
		EmployeeSite newEmpSite = new EmployeeSite(emp, stuttgart);
		newEmpSite.setStartDate(new Date());
		newEmpSite.setEndDate(date2030_12_31);
		
		eds.add(newEmpDept);
		ess.add(newEmpSite);
		
		employeeService.save(emp, eds, ess);
	}

	/**
	 * Gets the emp count.
	 *
	 * @return the emp count
	 */
	@Test
	public void getEmpCount() {
		assertThat(employeeService.count()).isEqualTo(5);
	}

	/**
	 * Test find current manager by department code.
	 *
	 * @throws NotFoundException
	 *             the not found exception
	 */
	@Test
	public void testFindCurrentManagerByDepartmentCode() throws NotFoundException {
		Employee currentManager = employeeService.findCurrentManagerByDepartmentCode(CMGO_DEPARTMENT_CODE);

		assertThat(currentManager.getEmployeeNo()).isEqualTo("EMP00002");
	}

	/**
	 * Test find manager by department at date.
	 *
	 * @throws ParseException
	 *             the parse exception
	 * @throws DuplicatedException
	 *             the duplicated exception
	 */
	@Test
	public void testFindManagerByDepartmentAtDate() throws ParseException, DuplicatedException {
		Date atDate = DateUtils.parseDate("2008-10-20", new String[] { Constants.YYYY_MM_DD_PATTERN });

		Employee managerAt2008 = employeeService.findManagerByDepartmentCodeAtDate(CMGO_DEPARTMENT_CODE, atDate);

		assertThat(managerAt2008.getEmployeeNo()).isEqualTo("EMP00003");

		Date at2016Date = DateUtils.parseDate("2016-03-20", Constants.YYYY_MM_DD_PATTERN_ARR);
		Employee managerAt2016 = employeeService.findManagerByDepartmentCodeAtDate(CMGO_DEPARTMENT_CODE, at2016Date);

		assertThat(managerAt2016.getEmployeeNo()).isEqualTo("EMP00002");
	}

	/**
	 * Test find current employees by department code.
	 * 
	 * Hong is current employee of CMGO from 1/1/2010 to present
	 * Hoh is Manager for CMGO from 1/1/2010 to present
	 * Wilson is current employee of CMGO from 1/1/2016 to present
	 * Expect Hoo Chooseng and Hong Nguyen, Wilson are currently belonged to CMGO
	 *
	 * @throws ParseException
	 *             the parse exception
	 * @throws DuplicatedException
	 *             the duplicated exception
	 */
	@Test
	public void testFindCurrentEmployeesByDepartmentCode() throws ParseException, DuplicatedException {

		List<Employee> employees = employeeService
				.findCurrentEmployeesByDepartmentCode(cmgoDepartment.getDepartmentCode());
		
		// We have Hoo Chooseng and Hong Nguyen, Wilson are currently belonged to CMGO
		assertThat(employees.size()).isEqualTo(3);
		assertThat(employees.get(0).getEmployeeNo()).isEqualTo(HOO_EMPLOYEE_NO);
	}

	/**
	 * Test find employees by department code at date.
	 *
	 * @throws ParseException
	 *             the parse exception
	 */
	@Test
	public void testFindEmployeesByDepartmentCodeAtDate() throws ParseException {
		Date date20150101 = DateUtils.parseDate("2015-01-01", Constants.YYYY_MM_DD_PATTERN_ARR);
		List<Employee> cmgoEmployeesAt20150101 = employeeService
				.findEmployeesByDepartmentCodeAtDate(CMGO_DEPARTMENT_CODE, date20150101);

		// expected only Hoo Choseng EMP00002 work from 2010-01-01 to present
		assertThat(cmgoEmployeesAt20150101.size()).isEqualTo(2);
	}

	/**
	 * Hoo is manager of CMGO from 2010-01-01 until now Dr Thed is manager of
	 * CMGO from 2000-01-01 to 2009-12-31, so currently Dr Thed doesn't work for
	 * CMGO anymore.
	 */
	@Test
	public void testIsEmployeeCurrentlyAssignedToDepartment() {
		boolean isHooAssignedToCMGO = employeeService.isEmployeeCurrentlyAssignedToDepartment(HOO_EMPLOYEE_NO,
				CMGO_DEPARTMENT_CODE);

		boolean isDrThedAssignedToCMGO = employeeService.isEmployeeCurrentlyAssignedToDepartment(DR_THED_EMPLOYEE_NO,
				CMGO_DEPARTMENT_CODE);

		assertThat(isHooAssignedToCMGO).isEqualTo(true);
		assertThat(isDrThedAssignedToCMGO).isEqualTo(false);

	}

	/**
	 * Test is employee assigned to department between.
	 *
	 * @throws ParseException
	 *             the parse exception
	 */
	@Test
	public void testIsEmployeeAssignedToDepartmentBetween() throws ParseException {

		Date date2001_02_14 = DateUtils.parseDate("2001-02-14", Constants.YYYY_MM_DD_PATTERN_ARR);

		// from 2000-01-01 to 2009-12-31
		boolean isDrThedAssignedToCMGOBetween19991230And20080501 = employeeService
				.isEmployeeAssignedToDepartmentAtDate(DR_THED_EMPLOYEE_NO, CMGO_DEPARTMENT_CODE, date2001_02_14);
		// from 2010-01-01 to present
		boolean isHooAssignedToCMGOBetween19991230And20080501 = employeeService
				.isEmployeeAssignedToDepartmentAtDate(HOO_EMPLOYEE_NO, CMGO_DEPARTMENT_CODE, date2001_02_14);

		assertThat(isDrThedAssignedToCMGOBetween19991230And20080501).isEqualTo(true);
		assertThat(isHooAssignedToCMGOBetween19991230And20080501).isEqualTo(false);

	}

	/**
	 * Test find current employees by site name.
	 */
	@Test
	public void testFindCurrentEmployeesBySiteName() {
		List<Employee> employees = employeeService.findCurrentEmployeesBySiteName(STUTTGART_SITE_NAME);

		Employee hooEmp = employeeService.findByEmployeeNo(HOO_EMPLOYEE_NO);
		Employee thedEmp = employeeService.findByEmployeeNo(DR_THED_EMPLOYEE_NO);

		assertThat(employees.size()).isEqualTo(2);
		assertThat(employees.contains(hooEmp)).isEqualTo(true);
		assertThat(employees.contains(thedEmp)).isEqualTo(true);

	}

	/**
	 * Test find employees by site name between.
	 *
	 * @throws ParseException
	 *             the parse exception
	 */
	@Test
	public void testFindEmployeesBySiteNameBetween() throws ParseException {

		// Hong emp: '2010-05-10', '2015-10-10'
		Date date2010_10_14 = DateUtils.parseDate("2010-10-14", Constants.YYYY_MM_DD_PATTERN_ARR);

		Employee hongEmp = employeeService.findByEmployeeNo(HONG_EMPLOYEE_NO);
		List<Employee> employees = employeeService.findEmployeesBySiteNameAtDate(STUTTGART_SITE_NAME, date2010_10_14);

		assertThat(employees).contains(hongEmp);

	}

}
