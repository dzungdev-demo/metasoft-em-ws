package com.metasoft.em.entity;

import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.metasoft.em.constant.Constants;

/**
 * The Class Employee.
 *
 * @author Alex Dang
 */
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Employee.class)
public class Employee extends AbstractPersistable<Long>{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 650157311766697083L;

	/** The employee no. */
	@Column(name="employee_no", length=20,nullable=false, unique = true)
	private String employeeNo;
	
	/** The ssn. */
	@NotBlank
	@Size(max = 50)
	@Column(name="ssn", length = 50, nullable=false, unique = true)
	private String ssn;
	
	/** The first name. */
	@NotBlank
	@Size(max = 100)
	@Column(name="first_name", length=100,nullable=false)
	private String firstName;
	
	/** The last name. */
	@NotBlank
	@Size(max = 100)
	@Column(name="last_name", length=100,nullable=false)
	private String lastName;
	
	/** The business fax number. */
	@NotBlank
	@Size(max = 20)
	@Column(name="business_fax_number", length=20,nullable=false)
	private String businessFaxNumber;
	
	/** The business phone number. */
	@NotBlank
	@Size(max = 15)
	@Column(name="business_phone_number", length=15,nullable=false)
	private String businessPhoneNumber;
	
	/** The telephone number. */
	@Size(max = 15)
	@Column(name="telephone_number", length=15)
	private String telephoneNumber;
	
	/** The mobile number. */
	@Size(max = 15)
	@Column(name="mobile_number", length=15)
	private String mobileNumber;
	
	/** The fax number. */
	@Size(max = 20)
	@Column(name="fax_number", length=20)
	private String faxNumber;
	
	/** The email. */
	@NotBlank
	@Size(max = 100)
	@Column(name="email", length=100, nullable=false, unique = true)
	private String email;
	
	/** The gender. */
	@NotBlank
	@Size(max = 1)
	@Column(name="gender", length = 1)
	private String gender;
	
	/** The dob. */
	@Column(name="dob")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dob;
	
	/** The entry date. */
	@NotNull
	@Column(name="entry_date", nullable = false)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date entryDate;
	
	/** The leaving date. */
	@NotNull
	@Column(name="leaving_date", nullable = false)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date leavingDate;
	
	/** The external. */
	@Column(name="is_external", length = 1)
	private String external;
	
	/** The address. */
	@OneToOne(cascade =  CascadeType.ALL)
	@JoinColumn(name="address_id", referencedColumnName="id")
	private Address address;
	
	/** The employee departments. */
//	@NotEmpty
	@OneToMany(mappedBy="employee", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<EmployeeDepartment> employeeDepartments;
	
	/** The employee sites. */
//	@NotEmpty
	@OneToMany(mappedBy="employee", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<EmployeeSite> employeeSites;

	/**
	 * Instantiates a new employee.
	 */
	public Employee() {}

	/**
	 * Instantiates a new employee.
	 *
	 * @param ssn the ssn
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param businessFaxNumber the business fax number
	 * @param businessPhoneNumber the business phone number
	 * @param telephoneNumber the telephone number
	 * @param mobileNumber the mobile number
	 * @param faxNumber the fax number
	 * @param email the email
	 * @param gender the gender
	 * @param dob the dob
	 * @param entryDate the entry date
	 * @param leavingDate the leaving date
	 * @param external the external
	 * @param address the address
	 */
	public Employee(String ssn, String firstName, String lastName, String businessFaxNumber,
			String businessPhoneNumber, String telephoneNumber, String mobileNumber, String faxNumber, String email,
			String gender, Date dob, Date entryDate, Date leavingDate, String external, Address address) {
		this.ssn = ssn;
		this.firstName = firstName;
		this.lastName = lastName;
		this.businessFaxNumber = businessFaxNumber;
		this.businessPhoneNumber = businessPhoneNumber;
		this.telephoneNumber = telephoneNumber;
		this.mobileNumber = mobileNumber;
		this.faxNumber = faxNumber;
		this.email = email;
		this.gender = gender;
		this.dob = dob;
		this.entryDate = entryDate;
		this.leavingDate = leavingDate;
		this.external = external;
		this.address = address;
	}

	/**
	 * Gets the employee no.
	 *
	 * @return the employee no
	 */
	public String getEmployeeNo() {
		return employeeNo;
	}

	/**
	 * Sets the employee no.
	 *
	 * @param employeeNo the new employee no
	 */
	public void setEmployeeNo(String employeeNo) {
		this.employeeNo = employeeNo;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the gender.
	 *
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * Sets the gender.
	 *
	 * @param gender the new gender
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * Gets the dob.
	 *
	 * @return the dob
	 */
	public Date getDob() {
		return dob;
	}

	/**
	 * Sets the dob.
	 *
	 * @param dob the new dob
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}

	/**
	 * Gets the entry date.
	 *
	 * @return the entry date
	 */
	public Date getEntryDate() {
		return entryDate;
	}

	/**
	 * Sets the entry date.
	 *
	 * @param entryDate the new entry date
	 */
	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	/**
	 * Gets the leaving date.
	 *
	 * @return the leaving date
	 */
	public Date getLeavingDate() {
		return leavingDate;
	}

	/**
	 * Sets the leaving date.
	 *
	 * @param leavingDate the new leaving date
	 */
	public void setLeavingDate(Date leavingDate) {
		this.leavingDate = leavingDate;
	}

	/**
	 * Gets the employee departments.
	 *
	 * @return the employee departments
	 */
	public List<EmployeeDepartment> getEmployeeDepartments() {
		return employeeDepartments;
	}

	/**
	 * Sets the employee departments.
	 *
	 * @param employeeDepartments the new employee departments
	 */
	public void setEmployeeDepartments(List<EmployeeDepartment> employeeDepartments) {
		this.employeeDepartments = employeeDepartments;
	}

	/**
	 * Gets the employee sites.
	 *
	 * @return the employee sites
	 */
	public List<EmployeeSite> getEmployeeSites() {
		return employeeSites;
	}

	/**
	 * Sets the employee sites.
	 *
	 * @param employeeSites the new employee sites
	 */
	public void setEmployeeSites(List<EmployeeSite> employeeSites) {
		this.employeeSites = employeeSites;
	}

	/**
	 * Gets the ssn.
	 *
	 * @return the ssn
	 */
	public String getSsn() {
		return ssn;
	}

	/**
	 * Sets the ssn.
	 *
	 * @param ssn the new ssn
	 */
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	/**
	 * Gets the business fax number.
	 *
	 * @return the business fax number
	 */
	public String getBusinessFaxNumber() {
		return businessFaxNumber;
	}

	/**
	 * Sets the business fax number.
	 *
	 * @param businessFaxNumber the new business fax number
	 */
	public void setBusinessFaxNumber(String businessFaxNumber) {
		this.businessFaxNumber = businessFaxNumber;
	}

	/**
	 * Gets the business phone number.
	 *
	 * @return the business phone number
	 */
	public String getBusinessPhoneNumber() {
		return businessPhoneNumber;
	}



	/**
	 * Sets the business phone number.
	 *
	 * @param businessPhoneNumber the new business phone number
	 */
	public void setBusinessPhoneNumber(String businessPhoneNumber) {
		this.businessPhoneNumber = businessPhoneNumber;
	}



	/**
	 * Gets the telephone number.
	 *
	 * @return the telephone number
	 */
	public String getTelephoneNumber() {
		return telephoneNumber;
	}



	/**
	 * Sets the telephone number.
	 *
	 * @param telephoneNumber the new telephone number
	 */
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}



	/**
	 * Gets the mobile number.
	 *
	 * @return the mobile number
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * Sets the mobile number.
	 *
	 * @param mobileNumber the new mobile number
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Gets the fax number.
	 *
	 * @return the fax number
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * Sets the fax number.
	 *
	 * @param faxNumber the new fax number
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * Gets the external.
	 *
	 * @return the external
	 */
	public boolean getExternal() {
		return Constants.BOOLEAN_YES.equals(external);
	}

	/**
	 * Sets the external.
	 *
	 * @param external the new external
	 */
	public void setExternal(boolean external) {
		this.external = external ? Constants.BOOLEAN_YES : Constants.BOOLEAN_NO;
	}
	
	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return firstName + " " + lastName;
	}
	
	/**
	 * Gets the all department code.
	 *
	 * @return the all department code
	 */
	public String getAllDepartmentCode() {
		
		if(employeeDepartments == null || 
				employeeDepartments.isEmpty()) {
			return "";
		} else {
			StringJoiner sj = new StringJoiner(Constants.COMMA);
			String allDepartmentCode = "";
			for (EmployeeDepartment ed : employeeDepartments) {
				if (!allDepartmentCode.contains(ed.getDepartmentCode())) {
					sj.add(ed.getDepartmentCode());
					allDepartmentCode = sj.toString();
				}
			}
			return allDepartmentCode;
		}
		
	}
	
	/**
	 * Gets the all site name.
	 *
	 * @return the all site name
	 */
	public String getAllSiteName() {
		
		if(employeeSites == null || 
				employeeSites.isEmpty()) {
			return "";
		} else {
			StringJoiner sj = new StringJoiner(Constants.COMMA);
			String allSiteName = "";
			for (EmployeeSite es : employeeSites) {
				if (!allSiteName.contains(es.getSiteName())) {
					sj.add(es.getSiteName());
					allSiteName = sj.toString();
				}
			}
			return allSiteName;
		}
	}
	
}