package com.metasoft.em.repository;

import com.metasoft.em.entity.Site;

/**
 * The Interface SiteRepository.
 *
 * @author Alex Dang
 */
public interface SiteRepository extends IRepository<Site> {
	
	/**
	 * Find by site name.
	 *
	 * @param siteName the site name
	 * @return the site
	 */
	public Site findBySiteName(String siteName);
	
}