package com.metasoft.em.repository;

import com.metasoft.em.entity.Address;

/**
 * The Interface AddressRepository.
 *
 * @author Alex Dang
 */
public interface AddressRepository extends IRepository<Address>{

}
