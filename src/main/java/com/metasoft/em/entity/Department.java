package com.metasoft.em.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.AbstractPersistable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * The Class Department.
 *
 * @author Alex Dang
 */
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Department.class)
public class Department extends AbstractPersistable<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3960321107228420522L;

	/** The department code. */
	@NotBlank
	@Size(max = 30)
	@Column(name = "department_code", length = 30, nullable = false, unique = true)
	private String departmentCode;
	
	/** The department name. */
	@NotBlank
	@Size(max = 100)
	@Column(name = "department_name", length = 100, nullable = false)
	private String departmentName;
	
	/** The parent id. */
	@Column(name = "parent_id")
	private Long parentId;

	/** The manager id. */
	@Column(name = "manager_id")
	private Long managerId;
	
	/** The manager name. */
	@Column(name="manager_name", length=200)
	private String managerName;
	
	/** The sub departments. */
	@OneToMany(mappedBy="parent")
	@JsonIgnore
	private List<Department> subDepartments;
	
	/** The parent. */
	@ManyToOne
	@JoinColumn(name="parent_id", referencedColumnName="id", insertable = false, updatable = false)
	@JsonIgnore
	private Department parent;

	/**
	 * Instantiates a new department.
	 */
	public Department() {
	}

	/**
	 * Gets the department code.
	 *
	 * @return the department code
	 */
	public String getDepartmentCode() {
		return departmentCode;
	}

	/**
	 * Sets the department code.
	 *
	 * @param departmentCode the new department code
	 */
	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	/**
	 * Gets the department name.
	 *
	 * @return the department name
	 */
	public String getDepartmentName() {
		return departmentName;
	}

	/**
	 * Sets the department name.
	 *
	 * @param departmentName the new department name
	 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	/**
	 * Gets the manager id.
	 *
	 * @return the manager id
	 */
	public Long getManagerId() {
		return managerId;
	}

	/**
	 * Sets the manager id.
	 *
	 * @param managerId the new manager id
	 */
	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Gets the parent id.
	 *
	 * @return the parent id
	 */
	public Long getParentId() {
		return parentId;
	}

	/**
	 * Sets the parent id.
	 *
	 * @param parentId the new parent id
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	/**
	 * Gets the sub departments.
	 *
	 * @return the sub departments
	 */
	public List<Department> getSubDepartments() {
		return subDepartments;
	}

	/**
	 * Sets the sub departments.
	 *
	 * @param subDepartments the new sub departments
	 */
	public void setSubDepartments(List<Department> subDepartments) {
		this.subDepartments = subDepartments;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public Department getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(Department parent) {
		this.parent = parent;
		if (parent != null) this.parentId = parent.getId();
	}

	/**
	 * Gets the manager name.
	 *
	 * @return the manager name
	 */
	public String getManagerName() {
		return managerName;
	}

	/**
	 * Sets the manager name.
	 *
	 * @param managerName the new manager name
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	
	/**
	 * Gets the parent name.
	 *
	 * @return the parent name
	 */
	public String getParentName() {
		return parent == null ? "" : parent.departmentName;
	}

}