package com.metasoft.em.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.metasoft.em.AbstractIntegrationTest;
import com.metasoft.em.entity.Department;
import com.metasoft.em.entity.Employee;
import com.metasoft.em.exception.DuplicatedException;
import com.metasoft.em.exception.NotFoundException;

/**
 * The Class DepartmentServiceIntTest.
 *
 * @author Alex Dang
 */
public class DepartmentServiceIntTest extends AbstractIntegrationTest {
	
	/** The employee service. */
	@Autowired
	private EmployeeService employeeService;
	
	/**
	 * Test update.
	 *
	 * @throws NotFoundException the not found exception
	 * @throws DuplicatedException the duplicated exception
	 */
	@Test
	public void testUpdate() throws NotFoundException, DuplicatedException {
		Department cmggDepartment = departmentService.findOne(1L);
		Employee thedEmp = employeeService.findOne(3L);
		
		cmggDepartment.setManagerId(thedEmp.getId());
		
		departmentService.update(cmggDepartment);
		
		cmggDepartment = departmentService.findOne(1L);
		assertThat(cmggDepartment.getManagerId()).isEqualTo(3);
		assertThat(cmggDepartment.getManagerId()).isEqualTo(thedEmp.getId());
		
	}
	
}
