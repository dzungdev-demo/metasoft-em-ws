package com.metasoft.em.repository;

import java.util.List;

import com.metasoft.em.entity.Department;

/**
 * The Interface DepartmentRepository.
 *
 * @author Alex Dang
 */
public interface DepartmentRepository extends IRepository<Department>{
	
	/**
	 * Find by department code.
	 *
	 * @param departmentCode the department code
	 * @return the department
	 */
	public Department findByDepartmentCode(String departmentCode);
	
	
	/**
	 * Find by manager id.
	 *
	 * @param managerId the manager id
	 * @return the department list
	 */
	public List<Department> findByManagerId(Long managerId);
	
}