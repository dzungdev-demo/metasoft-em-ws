package com.metasoft.em.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metasoft.em.entity.Employee;
import com.metasoft.em.entity.EmployeeSite;
import com.metasoft.em.entity.Site;
import com.metasoft.em.exception.ConflictException;
import com.metasoft.em.exception.DuplicatedException;
import com.metasoft.em.exception.NotFoundException;
import com.metasoft.em.repository.EmployeeRepository;
import com.metasoft.em.repository.EmployeeSiteRepository;
import com.metasoft.em.repository.IRepository;
import com.metasoft.em.repository.SiteRepository;
import com.metasoft.em.utils.DateUtil;

/**
 * The Class EmployeeSiteService.
 *
 * @author Alex Dang
 */
@Service
@Transactional
public class EmployeeSiteService extends AbstractService<EmployeeSite> {

	/** The employee site repository. */
	@Autowired
	private EmployeeSiteRepository employeeSiteRepository;
	
	/** The employee repository. */
	@Autowired
	private EmployeeRepository employeeRepository;
	
	/** The site repository. */
	@Autowired
	private SiteRepository siteRepository;

	/**
	 * This method is provide the IRepository for based method.
	 *
	 * @return the repository
	 */
	@Override
	public IRepository<EmployeeSite> getRepository() {
		return employeeSiteRepository;
	}

	/**
	 * This method is used to assign one Employee to Site
	 * 
	 * We will need to check: 
	 * 1. entryDate < startDate < leaving date || leavingDate is null 
	 * 2. Checking any record of this employee in database has overlap with startDate and endDate 
	 * 3. If no overlapse, then we need to find one record of employee this have end date is null 
	 * 4. if there is one record like that, then we need to check whether the manager is change
	 * or not if it is same with new one, then don't allow to save 
	 * 5. If it is not same, then allow 	to createNew and update the endDate of previous
	 * record 
	 * 6. Check to see update or not for managerId in Site
	 *
	 * @param es            the employee Site
	 * @return the employee Site
	 * @throws DuplicatedException             the duplicated exception
	 * @throws ConflictException the conflict exception
	 */
	public EmployeeSite save(EmployeeSite es) throws DuplicatedException, ConflictException {
		
		if (!DateUtil.isValidStartDateEndDate(es.getStartDate(), es.getEndDate())) {
			throw new ConflictException(msg("start.date.can.not.bigger.than.end.date"));
		}
		
		Employee emp = employeeRepository.findOne(es.getEmployeeId());
		Site site = siteRepository.findOne(es.getSiteId());
		es.setEmployee(emp);
		es.setSite(site);
		
		validateStartEndDateWithEntryAndLeavingDate(es, emp.getEntryDate(), emp.getLeavingDate());
		validateOverlapseDate(es, true);
		
		updateEndDateSameEmployeeForPreviousPeriod(es);
		// Now we can create employeeSite
		employeeSiteRepository.save(es);

		return es;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.metasoft.em.service.AbstractService#update(org.springframework.data.
	 * jpa.domain.AbstractPersistable)
	 */
	public EmployeeSite update(EmployeeSite es) throws NotFoundException, DuplicatedException, ConflictException {
		if (!DateUtil.isValidStartDateEndDate(es.getStartDate(), es.getEndDate())) {
			throw new ConflictException(msg("start.date.can.not.bigger.than.end.date"));
		}
		EmployeeSite existingEmpSite = getRepository().findOne(es.getId());
		if (existingEmpSite == null) {
			throw new NotFoundException();
		}
		//check overlapse date record
		Employee emp = employeeRepository.findOne(es.getEmployeeId());
		es.setEmployee(emp);
		Site site = siteRepository.findOne(es.getSiteId());
		es.setSite(site);
		
		
		validateStartEndDateWithEntryAndLeavingDate(es, emp.getEntryDate(), emp.getLeavingDate());
		validateOverlapseDate(es, false);
		
		updateEndDateSameEmployeeForPreviousPeriod(es);
		// Now we can create employeeDepartmentRepository
		BeanUtils.copyProperties(es, existingEmpSite);
		employeeSiteRepository.save(existingEmpSite);
		
		return existingEmpSite;
	}
	
	/**
	 * Update end date same employee for previous period.
	 *
	 * @param es the ed
	 */
	private void updateEndDateSameEmployeeForPreviousPeriod(EmployeeSite es) {
		// no overlapsed, so we find any record which has endDate is null to update
		List<EmployeeSite> sameEmpForSiteButEndDateNullESs = 
				employeeSiteRepository.findSameEmployeeSiteOrderByStartDateDesc(
												es.getEmployeeId(), es.getSiteId(), es.getStartDate());
		// update end date of previous button to be the date 
		if (!sameEmpForSiteButEndDateNullESs.isEmpty()) {
			EmployeeSite sameEmpForSiteButEndDateNullED = sameEmpForSiteButEndDateNullESs.get(0);
			sameEmpForSiteButEndDateNullED.setEndDate(DateUtils.addDays(es.getStartDate(), -1));
			employeeSiteRepository.save(sameEmpForSiteButEndDateNullED);
		}
	}
	
	/**
	 * Validate overlapse date.
	 *
	 * @param es the EmployeeSite
	 * @param isCreate the is create
	 * @throws DuplicatedException the duplicated exception
	 */
	public void validateOverlapseDate(EmployeeSite es, boolean isCreate) throws DuplicatedException {
		//checking overlapsed
		List<EmployeeSite> overlapsedESs = new ArrayList<EmployeeSite>();
		if (isCreate) {
			overlapsedESs = employeeSiteRepository.findOverlapsedEmpSite(
															es.getEmployeeId(), 
															es.getSiteId(), 
															es.getStartDate(),
															es.getEndDate());
		} else {
			overlapsedESs = employeeSiteRepository.findOverlapsedEmpSiteExceptId(
															es.getEmployeeId(), 
															es.getSiteId(), 
															es.getStartDate(),
															es.getEndDate(),
															es.getId());
		}				
		if (!overlapsedESs.isEmpty()) {
			throw new DuplicatedException(msg("employee.edit.department.site.overlapsed"));
		}
	}
	
	
	/**
	 * Validate start end date with entry and leaving date.
	 *
	 * @param ed the ed
	 * @param entryDate the entry date
	 * @param leavingDate the leaving date
	 * @throws DuplicatedException the duplicated exception
	 */
	private void validateStartEndDateWithEntryAndLeavingDate(
					EmployeeSite ed, Date entryDate, Date leavingDate) throws DuplicatedException {
		// if ed.startDate before entryDate or ed.startDate after leavingDate
		// then we need to throw error
		if (ed.getStartDate().before(entryDate)
				|| (leavingDate != null && leavingDate.before(ed.getStartDate()))) {
			throw new DuplicatedException(msg("start.date.conflict.entry.leaving.date"));
		}
	}

	/**
	 * Find by employee id.
	 *
	 * @param employeeId the employee id
	 * @return the list
	 */
	public List<EmployeeSite> findByEmployeeId(Long employeeId) {
		return employeeSiteRepository.findByEmployeeId(employeeId);
	}
	
	/**
	 * Delete by employee id and site id.
	 *
	 * @param employeeId the employee id
	 * @param siteId the site id
	 * @return the long
	 */
	public Long deleteByEmployeeIdAndSiteId(Long employeeId, Long siteId) {
		return employeeSiteRepository.deleteByEmployeeIdAndSiteId(employeeId, siteId);
	}

	/**
	 * Checks if is overlapsed between StartDate and EndDate of 
	 * EmployeeSite List.
	 *
	 * @param ess the list of EmployeeSite
	 * @return true, if is overlapsed
	 */
	public boolean isOverlapsed(List<EmployeeSite> ess) {
		if (ess.size() < 2) return false;
		
		Map<Long, List<EmployeeSite>> essMap = 
										ess.stream()
										   .collect(
												Collectors.groupingBy(EmployeeSite::getSiteId));
		for (Long siteId: essMap.keySet()) {
			List<EmployeeSite> empSites = essMap.get(siteId);
			empSites.stream()
			   .sorted((ed1, ed2) -> ed1.getStartDate().compareTo(ed2.getStartDate()));
			for (int i = 0; i <= ess.size() - 2; i++) {
				EmployeeSite ed1 = empSites.get(i);
				EmployeeSite ed2 = empSites.get(i+1);
				if (ed1.getEndDate() == null ||
						ed1.getEndDate().after(ed2.getStartDate())) { 
							return true;
				}			
			}
		}
		return false;
	}

}
