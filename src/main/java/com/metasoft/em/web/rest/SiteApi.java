package com.metasoft.em.web.rest;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metasoft.em.entity.Employee;
import com.metasoft.em.entity.Site;
import com.metasoft.em.exception.DuplicatedException;
import com.metasoft.em.exception.NotFoundException;
import com.metasoft.em.service.EmployeeService;
import com.metasoft.em.service.SiteService;

/**
 * The Class SiteApi.
 *
 * @author Alex Dang
 */
@RestController
@RequestMapping(value = "/api/v1/sites")
public class SiteApi {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(SiteApi.class);

	/** The site service. */
	@Autowired
	private SiteService siteService;

	/** The employee service. */
	@Autowired
	private EmployeeService employeeService;

	/**
	 * Find all.
	 *
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Site>> findAll() {
		List<Site> sites = siteService.findAll();
		if (sites.isEmpty()) {
			return new ResponseEntity<List<Site>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Site>>(sites, HttpStatus.OK);
	}

	/**
	 * Find by id.
	 *
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Site> findById(@PathVariable("id") long id) {
		Site site = siteService.findOne(id);
		if (site == null) {
			return new ResponseEntity<Site>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Site>(site, HttpStatus.OK);
	}

	/**
	 * Creates a site.
	 *
	 * @param site
	 *            the site
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> create(@Validated @RequestBody Site site) {
		try {
			siteService.save(site);
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		} catch (DuplicatedException de) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}

	/**
	 * Update site.
	 *
	 * @param site
	 *            the site
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@Validated @RequestBody Site site) {
		try {
			siteService.update(site);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (NotFoundException nfe) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} catch(DuplicatedException de) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}

	/**
	 * Delete site.
	 *
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Site> delete(@PathVariable("id") long id) {
		try {
			siteService.delete(id);
		} catch (NotFoundException nfe) {
			return new ResponseEntity<Site>(HttpStatus.NOT_FOUND);
		} catch (DataIntegrityViolationException dive) {
			log.debug("CONFLICT in deleting department with department id {}", id);
			return new ResponseEntity<Site>(HttpStatus.CONFLICT);
		}
		return new ResponseEntity<Site>(HttpStatus.OK);
	}

	/**
	 * Find current employees by site name.
	 *
	 * @param siteName
	 *            the site name
	 * @return the response entity
	 */
	@RequestMapping(value = "/{siteName}/employees/current", method = RequestMethod.GET)
	public ResponseEntity<List<Employee>> findCurrentEmployeesBySiteName(@PathVariable("siteName") String siteName) {
		log.debug("start findCurrentEmployeesBySiteName for site name {}", siteName);
		List<Employee> employees = employeeService.findCurrentEmployeesBySiteName(siteName);
		
		if (employees.isEmpty()) {
			log.debug("end findCurrentEmployeesBySiteName: NO_CONTENT:204 for site name {}", siteName);
			return new ResponseEntity<List<Employee>>(HttpStatus.NO_CONTENT);
		} else {
			log.debug("end findCurrentEmployeesBySiteName: OK:200 for site name {}", siteName);
			return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
		}
	}

	/**
	 * Find employees by site name at date.
	 * @param siteName
	 *            the site name
	 * @param atDate
	 *            the at date
	 * @return the response entity
	 */
	@RequestMapping(value = "/{siteName}/employees/{atDate}", method = RequestMethod.GET)
	public ResponseEntity<List<Employee>> findEmployeesBySiteNameAtDate(
											@PathVariable("siteName") String siteName,
											@Valid @PathVariable("atDate") @DateTimeFormat(iso=ISO.DATE) Date atDate) {
		log.debug("start findEmployeesBySiteNameAtDate for site name {} at date {}", siteName, atDate);
		
		List<Employee> employees = employeeService.findEmployeesBySiteNameAtDate(siteName, atDate);
		if (employees.isEmpty()) {
			log.debug("end findEmployeesBySiteNameAtDate: NO_CONTENT:204 for site name {} at date {}", siteName, atDate);
			
			return new ResponseEntity<List<Employee>>(HttpStatus.NO_CONTENT);
		} else {
			log.debug("end findEmployeesBySiteNameAtDate: OK:200 for site name {} at date {}", siteName, atDate);
			
			return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
		}
	}
	
	
}
