package com.metasoft.em.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.AbstractPersistable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.metasoft.em.constant.Constants;

/**
 * The Class Site.
 *
 * @author Alex Dang
 */
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Site.class)
public class Site extends AbstractPersistable<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5528660027236789373L;

	/** The site name. */
	@NotBlank
	@Size(max = 100)
	@Column(name = "site_name", length = 100, nullable = false, unique = true)
	private String siteName;

	/** The latitude. */
	@NotNull
	@Column(name = "latitude")
	private double latitude;

	/** The longitude. */
	@NotNull
	@Column(name = "longitude")
	private double longitude;

	/** The external. */
	@Column(name = "is_external", length = 1)
	private String external;

	/** The address. */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id", referencedColumnName = "id")
	private Address address;

	/** The address id. */
	@Column(name = "address_id", insertable = false, updatable = false)
	private Long addressId;

	/**
	 * Instantiates a new site.
	 */
	public Site() {
	}

	/**
	 * Instantiates a new site.
	 *
	 * @param siteName            the site name
	 * @param latitude            the latitude
	 * @param longitude            the longitude
	 * @param external            the external
	 * @param address            the address
	 */
	public Site(String siteName, double latitude, double longitude, String external, Address address) {
		this.siteName = siteName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.external = external;
		this.address = address;
		if (this.address != null)
			this.addressId = this.address.getId();
	}

	/**
	 * Gets the site name.
	 *
	 * @return the site name
	 */
	public String getSiteName() {
		return siteName;
	}

	/**
	 * Sets the site name.
	 *
	 * @param siteName
	 *            the new site name
	 */
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	/**
	 * Gets the latitude.
	 *
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * Sets the latitude.
	 *
	 * @param latitude
	 *            the new latitude
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * Gets the longitude.
	 *
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * Sets the longitude.
	 *
	 * @param longitude
	 *            the new longitude
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * Gets the external.
	 *
	 * @return the external
	 */
	public boolean getExternal() {
		return Constants.BOOLEAN_YES.equals(external);
	}

	/**
	 * Sets the external.
	 *
	 * @param external
	 *            the new external
	 */
	public void setExternal(boolean external) {
		this.external = external ? Constants.BOOLEAN_YES : Constants.BOOLEAN_NO;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address
	 *            the new address
	 */
	public void setAddress(Address address) {
		this.address = address;
		if (address != null && !address.isNew())
			this.addressId = address.getId();
	}

	/**
	 * Gets the address id.
	 *
	 * @return the address id
	 */
	public Long getAddressId() {
		return addressId;
	}

	/**
	 * Sets the address id.
	 *
	 * @param addressId
	 *            the new address id
	 */
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	/**
	 * Gets the full address.
	 *
	 * @return the full address
	 */
	public String getFullAddress() {
		return address == null ? ""
				: (address.getHouseNumber() == null ? "" : address.getHouseNumber() + " ")
						+ (address.getStreet() == null ? "" : address.getStreet() + " ")
						+ (address.getCity() == null ? "" : address.getCity() + " ")
						+ (address.getPostcode() == null ? "" : address.getPostcode());
	}

	/**
	 * Gets the geo location.
	 *
	 * @return the geo location
	 */
	public String getGeoLocation() {
		return String.valueOf(latitude) + " : " + String.valueOf(longitude);
	}

}
