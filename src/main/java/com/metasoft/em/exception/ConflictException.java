package com.metasoft.em.exception;

/**
 * The Class ConflictException.
 *
 * @author Alex Dang
 */
public class ConflictException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7345953021716180344L;

	/**
	 * Instantiates a new conflict exception.
	 */
	public ConflictException() {
		super();
	}

	/**
	 * Instantiates a new conflict exception.
	 *
	 * @param message the message
	 */
	public ConflictException(String message) {
		super(message);
	}

}
