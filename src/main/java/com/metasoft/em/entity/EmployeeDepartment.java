package com.metasoft.em.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.metasoft.em.constant.Constants;

/**
 * The Class EmployeeDepartment.
 *
 * @author Alex Dang
 */
@Entity
@Table(name="employee_department")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EmployeeDepartment.class)
public class EmployeeDepartment extends AbstractPersistable<Long>{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6906861034018856355L;

	/** The employee id. */
	@Column(name="employee_id", nullable = false)
	private Long employeeId;
	
	/** The department id. */
	@NotNull
	@Column(name="department_id", nullable = false)
	private Long departmentId;
	
	/** The manager. */
	@Column(name="is_manager", length = 1)
	private String manager = Constants.BOOLEAN_NO;
	
	/** The start date. */
	@NotNull
	@Column(name="start_date", nullable = false)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date startDate;
	
	/** The end date. */
	@NotNull
	@Column(name="end_date", nullable=false)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date endDate;
	
	/** The employee. */
	@ManyToOne
	@JoinColumn(name="employee_id", referencedColumnName="id", insertable = false, updatable = false)
	@JsonIgnore
	private Employee employee;
	
	/** The department. */
	@ManyToOne
	@JoinColumn(name="department_id", referencedColumnName="id", insertable = false, updatable = false)
	@JsonIgnore
	private Department department;

	/**
	 * Instantiates a new employee department.
	 */
	public EmployeeDepartment() {}

	/**
	 * Instantiates a new employee department.
	 *
	 * @param employee the employee
	 * @param department the department
	 */
	public EmployeeDepartment(Employee employee, Department department) {
		this.employee = employee;
		this.department = department;
		this.employeeId = employee.getId();
		this.departmentId = department.getId();
	}

	/**
	 * Gets the employee id.
	 *
	 * @return the employee id
	 */
	public Long getEmployeeId() {
		return employeeId;
	}

	/**
	 * Sets the employee id.
	 *
	 * @param employeeId the new employee id
	 */
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * Gets the employee.
	 *
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * Sets the employee.
	 *
	 * @param employee the new employee
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
		if (employee != null) this.employeeId = employee.getId();
	}

	/**
	 * Gets the department id.
	 *
	 * @return the department id
	 */
	public Long getDepartmentId() {
		return departmentId;
	}

	/**
	 * Sets the department id.
	 *
	 * @param departmentId the new department id
	 */
	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public Department getDepartment() {
		return department;
	}

	/**
	 * Sets the department.
	 *
	 * @param department the new department
	 */
	public void setDepartment(Department department) {
		this.department = department;
		if (department != null && !department.isNew()) this.departmentId = department.getId();
	}

	/**
	 * Gets the manager.
	 *
	 * @return the manager
	 */
	public boolean isManager() {
		return Constants.BOOLEAN_YES.equals(manager);
	}

	/**
	 * Sets the manager.
	 *
	 * @param manager the new manager
	 */
	public void setManager(boolean manager) {
		this.manager = manager ? Constants.BOOLEAN_YES : Constants.BOOLEAN_NO;
	}

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Gets the department name.
	 *
	 * @return the department name
	 */
	public String getDepartmentName() {
		return department == null ? "" : department.getDepartmentName();
	}
	
	/**
	 * Gets the department code.
	 *
	 * @return the department code
	 */
	public String getDepartmentCode() {
		return department == null ? "" : department.getDepartmentCode();
	}
	
}