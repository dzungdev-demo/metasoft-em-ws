package com.metasoft.em.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.metasoft.em.entity.Employee;

/**
 * The Interface EmployeeRepository.
 *
 * @author Alex Dang
 */
public interface EmployeeRepository extends IRepository<Employee>{
	
	
	/**
	 * Find by ssn or email.
	 *
	 * @param ssn the ssn
	 * @param email the email
	 * @return the employee
	 */
	public Employee findBySsnOrEmail(String ssn, String email);
	
	/**
	 * Find by ssn.
	 *
	 * @param ssn the ssn
	 * @return the employee
	 */
	public Employee findBySsn(String ssn);
	
	/**
	 * Find by email.
	 *
	 * @param email the email
	 * @return the employee
	 */
	public Employee findByEmail(String email);
	
	/**
	 * Find first by order by employee no desc.
	 *
	 * @return the employee
	 */
	public Employee findFirstByOrderByEmployeeNoDesc();
	
	/**
	 * Find by employee no.
	 *
	 * @param employeeNo the employee no
	 * @return the employee
	 */
	public Employee findByEmployeeNo(String employeeNo);
	
	/**
	 * Find manager by department code at date.
	 *
	 * @param departmentCode the department code
	 * @param atDate the at date
	 * @return the list
	 */
	@Query("select e from Employee e "
			+ "	inner join e.employeeDepartments ed "
			+ " inner join ed.department dept "
			+ " where dept.departmentCode = :departmentCode and ed.manager = 'Y' "
			+ " and :atDate between ed.startDate and ed.endDate")
	public List<Employee> findManagerByDepartmentCodeAtDate(
														@Param("departmentCode") String departmentCode, 
														@Param("atDate") Date atDate);
	
	
	/**
	 * Find current employees by department code.
	 *
	 * @param departmentCode the department code
	 * @return the list
	 */
	@Query("select e from Employee e"
			+ "	inner join e.employeeDepartments ed "
			+ " inner join ed.department dept "
			+ " where dept.departmentCode = :departmentCode "
			+ " and e.leavingDate > CURRENT_DATE "
			+ " and ed.endDate > CURRENT_DATE")
	public List<Employee> findCurrentEmployeesByDepartmentCode(
														@Param("departmentCode") String departmentCode);
	
	/**
	 * Find employees by department code at date.
	 *
	 * @param departmentCode the department code
	 * @param atDate the at date
	 * @return the list
	 */
	@Query("select e from Employee e"
			+ "	inner join e.employeeDepartments ed "
			+ " inner join ed.department dept "
			+ " where dept.departmentCode = :departmentCode "
			+ " and :atDate between ed.startDate and ed.endDate ")
	public List<Employee> findEmployeesByDepartmentCodeAtDate(
														@Param("departmentCode") String departmentCode, 
														@Param("atDate") Date atDate);
	
	/**
	 * Find employee currently assigned to department.
	 *
	 * @param employeeNo the employee no
	 * @param departmentCode the department code
	 * @return the list
	 */
	@Query("select e from Employee e"
			+ "	inner join e.employeeDepartments ed "
			+ " inner join ed.department dept "
			+ " where e.employeeNo = :employeeNo and dept.departmentCode = :departmentCode "
			+ " and e.leavingDate > CURRENT_DATE "
			+ " and ed.endDate > CURRENT_DATE")
	public List<Employee> findEmployeeCurrentlyAssignedToDepartment(
															@Param("employeeNo") String employeeNo, 
															@Param("departmentCode") String departmentCode);
	
	/**
	 * Find specific employee assigned to department at date.
	 *
	 * @param employeeNo the employee no
	 * @param departmentCode the department code
	 * @param atDate the at date
	 * @return the list
	 */
	@Query("select e from Employee e"
			+ "	inner join e.employeeDepartments ed "
			+ " inner join ed.department dept "
			+ " where e.employeeNo = :employeeNo and dept.departmentCode = :departmentCode "
			+ " and :atDate between ed.startDate and ed.endDate ")
	public List<Employee> findSpecificEmployeeAssignedToDepartmentAtDate(
														@Param("employeeNo") String employeeNo, 
														@Param("departmentCode") String departmentCode, 
														@Param("atDate") Date atDate);
	
	/**
	 * Find employees by site name at date.
	 *
	 * @param siteName the site name
	 * @param atDate the at date
	 * @return the list
	 */
	@Query("select e from Employee e"
			+ "	inner join e.employeeSites es "
			+ " inner join es.site st "
			+ " where st.siteName = :siteName "
			+ " and :atDate between es.startDate and es.endDate ")
	public List<Employee> findEmployeesBySiteNameAtDate(
														@Param("siteName") String siteName,
														@Param("atDate") Date atDate);
	
}
