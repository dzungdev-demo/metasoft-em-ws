package com.metasoft.em.web.rest;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metasoft.em.constant.Constants;
import com.metasoft.em.entity.Employee;
import com.metasoft.em.entity.EmployeeDepartment;
import com.metasoft.em.entity.EmployeeSite;
import com.metasoft.em.exception.ConflictException;
import com.metasoft.em.exception.DuplicatedException;
import com.metasoft.em.exception.NotFoundException;
import com.metasoft.em.service.EmployeeDepartmentService;
import com.metasoft.em.service.EmployeeService;
import com.metasoft.em.service.EmployeeSiteService;
import com.metasoft.em.web.dto.EmployeeDTO;
import com.metasoft.em.web.dto.error.ErrorDTO;

/**
 * The Class EmployeeApi.
 *
 * @author Alex Dang
 */
@RestController
@RequestMapping(value = "/api/v1/employees")
public class EmployeeApi {
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(EmployeeApi.class);
	
	/** The employee service. */
	@Autowired
	private EmployeeService employeeService;
	
	/** The employee department service. */
	@Autowired
	private EmployeeDepartmentService employeeDepartmentService;
	
	/** The employee site service. */
	@Autowired
	private EmployeeSiteService employeeSiteService;

	/**
	 * Find all.
	 *
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Employee>> findAll() {
		log.debug("start end findAll for employee");
		List<Employee> employees = employeeService.findAll();
		if (employees.isEmpty()) {
			log.debug("end findAll for employee NO_CONTENT:204");
			return new ResponseEntity<List<Employee>>(HttpStatus.NO_CONTENT);
		}
		log.debug("end findAll for employee: OK:200");
		return new ResponseEntity<>(employees, HttpStatus.OK);
	}

	/**
	 * Find by id.
	 *
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Employee> findById(@PathVariable("id") long id) {
		log.debug("start findById with employee id {}", id);
		Employee employee = employeeService.findOne(id);
		if (employee == null) {
			log.debug("end findById: NO_CONTENT:204  with employee id {}", id);
			return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
		}
		log.debug("end findById: OK:200 with employee id {}", id);
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	}

	/**
	 * Creates the transaction.
	 *
	 * @param empDTO the emp DTO
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> create(
				@Valid @RequestBody EmployeeDTO empDTO) {
		Employee employee = empDTO.getEmployee();
		log.debug("start createEmployee with employee ssn {}", employee.getSsn());
		
		try {
			employeeService.save(employee, empDTO.getEmployeeDepartments(), empDTO.getEmployeeSites());
			
			log.debug("end createEmployee: CREATED:201 with employee ssn {}", employee.getSsn());
			return new ResponseEntity<Object>(employee, HttpStatus.CREATED);
		} catch (DuplicatedException | ConflictException ex) {
			log.debug("end createEmployee: CONFLICT:409 with employee ssn {}", employee.getSsn());
			
			return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorDTO(ex.getMessage(), HttpStatus.CONFLICT.value()));
		} catch (ParseException pe) {
			log.debug("end createEmployee: INTERNAL_SERVER_ERROR:500 with employee ssn {}", employee.getSsn());
			
			return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}

	/**
	 * Update employee.
	 *
	 * @param employee            the employee
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> update(@Validated @RequestBody Employee employee) {
		log.debug("start updateEmployee with employeeId {}", employee.getId());
		try {
			employeeService.update(employee);
			log.debug("end updateEmployee OK:200 with employeeId {}", employee.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} catch (NotFoundException nfe) {
			log.debug("end updateEmployee NOT_FOUND:404 with employeeId {}", employee.getId());
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
								 .body(new ErrorDTO(nfe.getMessage(), HttpStatus.NOT_FOUND.value()));
		} catch (DuplicatedException | ConflictException de) {
			log.debug("end updateEmployee CONFLICT: 409 with employeeId {}", employee.getId());
			return ResponseEntity.status(HttpStatus.CONFLICT)
								 .body(new ErrorDTO(de.getMessage(), HttpStatus.CONFLICT.value()));
		}
	}

	/**
	 * Delete employee.
	 *
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Employee> delete(@PathVariable("id") long id) {
		log.debug("start deleteEmployee with employeeId {}", id);
		try {
			employeeService.delete(id);
		} catch (NotFoundException nfe) {
			log.debug("end deleteEmployee: NOT_FOUND:404 with employeeId {}", id);
			return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
		}
		log.debug("end deleteEmployee: OK:200 with employeeId {}", id);
		return new ResponseEntity<Employee>(HttpStatus.OK);
	}
	
	/**
	 * Find employee department by employee.
	 *
	 * @param employeeId the employee id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}/employeeDepartments", method = RequestMethod.GET)
	public ResponseEntity<List<EmployeeDepartment>> findEmployeeDepartmentByEmployee(@PathVariable("id") Long employeeId) {
		log.debug("start findEmployeeDepartmentByEmployee with employeeId {}", employeeId);
		List<EmployeeDepartment> employeeDepartments = employeeDepartmentService.findByEmployeeId(employeeId);
		if (employeeDepartments.isEmpty()) {
			log.debug("end findEmployeeDepartmentByEmployee: NO_CONTENT:204 with employeeId {}", employeeId);
			return new ResponseEntity<List<EmployeeDepartment>>(HttpStatus.NO_CONTENT);
		}
		log.debug("end findEmployeeDepartmentByEmployee: OK:200 with employeeId {}", employeeId);
		return new ResponseEntity<List<EmployeeDepartment>>(employeeDepartments, HttpStatus.OK);
	}
	
	/**
	 * Find employeeDepartment by id.
	 *
	 * @param employeeDepartmentId the employeeDepartment id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{employeeId}/employeeDepartments/{id}", method = RequestMethod.GET)
	public ResponseEntity<EmployeeDepartment> findEmployeeDepartmentById(@PathVariable("id") Long employeeDepartmentId) {
		log.debug("start findEmployeeDepartmentById with employeeDepartmentId {}", employeeDepartmentId);
		EmployeeDepartment employeeDepartment = employeeDepartmentService.findOne(employeeDepartmentId);
		if (employeeDepartment == null) {
			log.debug("end findEmployeeDepartmentById: NO_CONTENT:204 with employeeDepartmentId {}", employeeDepartmentId);
			return new ResponseEntity<EmployeeDepartment>(HttpStatus.NO_CONTENT);
		}
		log.debug("end findEmployeeDepartmentById: OK:200 with employeeDepartmentId {}", employeeDepartmentId);
		return new ResponseEntity<EmployeeDepartment>(employeeDepartment, HttpStatus.OK);
	}
	
	/**
	 * Find employee sites by employee id.
	 *
	 * @param employeeId the employee id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}/employeeSites", method = RequestMethod.GET)
	public ResponseEntity<List<EmployeeSite>> findEmployeeSitesByEmployeeId(@PathVariable("id") Long employeeId) {
		log.debug("start findEmployeeSitesByEmployeeId by employeeId {}", employeeId);
		List<EmployeeSite> employeeSites = employeeSiteService.findByEmployeeId(employeeId);
		if (employeeSites.isEmpty()) {
			log.debug("end findEmployeeSitesByEmployeeId: NO_CONTENT:204 by employeeId {}", employeeId);
			return new ResponseEntity<List<EmployeeSite>>(HttpStatus.NO_CONTENT);
		}
		log.debug("end findEmployeeSitesByEmployeeId: OK:200 by employeeId {}", employeeId);
		return new ResponseEntity<List<EmployeeSite>>(employeeSites, HttpStatus.OK);
	}
	
	/**
	 * Find employeeSite by id.
	 *
	 * @param employeeSiteId the employeeSite id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{employeeId}/employeeSites/{id}", method = RequestMethod.GET)
	public ResponseEntity<EmployeeSite> findEmployeeSiteById(@PathVariable("id") Long employeeSiteId) {
		log.debug("start findEmployeeSiteById with employeeSiteId {}", employeeSiteId);
		EmployeeSite employeeSite = employeeSiteService.findOne(employeeSiteId);
		if (employeeSite == null) {
			log.debug("end findEmployeeSiteById: NO_CONTENT:204 with employeeSiteId {}", employeeSiteId);
			return new ResponseEntity<EmployeeSite>(HttpStatus.NO_CONTENT);
		}
		log.debug("end findEmployeeSiteById: OK:200 with employeeSiteId {}", employeeSiteId);
		return new ResponseEntity<EmployeeSite>(employeeSite, HttpStatus.OK);
	}
	
	/**
	 * Download CSV.
	 *
	 * @param response the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@RequestMapping(value="/csv", method = RequestMethod.GET)
	public void downloadCSV(HttpServletResponse response) throws IOException {
		
		log.debug("start exporting csv");
		 response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
		 String reportName = "employee-" + new Date().getTime() + ".csv";
		 response.setHeader("Content-disposition", "attachment;filename=" + reportName);
		
		ServletOutputStream os = response.getOutputStream();
		StringBuilder columnNames = new StringBuilder("Employee Number").append(Constants.COMMA)
											.append("Employee Full Name").append(Constants.COMMA)
											.append("Employee Type").append(Constants.COMMA)
											.append("Entry Date").append(Constants.COMMA)
											.append("Leaving Date");
		os.println(columnNames.toString());
		
		List<Employee> employees = employeeService.findAll();
		employees.forEach(emp -> {
			StringBuilder line = new StringBuilder();
			String empTypeLabel = 
					emp.getExternal() ? Constants.EMPLOYEE_TYPE_EXTERNAL : Constants.EMPLOYEE_TYPE_INTERNAL;
			String leavingDate = 
					emp.getLeavingDate() == null ? "" : Constants.DD_MM_YYYY_DATE_FORMAT.format(emp.getLeavingDate());
			line.append(emp.getEmployeeNo()).append(Constants.COMMA)
				.append(emp.getFirstName()).append(" ").append(emp.getLastName()).append(Constants.COMMA)
				.append(empTypeLabel).append(Constants.COMMA)
				.append(Constants.DD_MM_YYYY_DATE_FORMAT.format(emp.getEntryDate())).append(Constants.COMMA)
				.append(leavingDate);
				try {
					os.println(line.toString());
				} catch (IOException e) {
					log.debug("IOException in export csv: " + e.getMessage());
				}
		});
		os.flush();
		log.debug("end exporting csv");
	}
}
