package com.metasoft.em.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metasoft.em.entity.Department;
import com.metasoft.em.exception.DuplicatedException;
import com.metasoft.em.exception.NotFoundException;
import com.metasoft.em.repository.DepartmentRepository;
import com.metasoft.em.repository.IRepository;

/**
 * The Class DepartmentService.
 *
 * @author Alex Dang
 */
@Service
@Transactional
public class DepartmentService extends AbstractService<Department>{
	
	/** The department repository. */
	@Autowired
	private DepartmentRepository departmentRepository;

	/**
	 * This method is provide the IRepository for based method.
	 *
	 * @return the IRepository
	 */
	@Override
	public IRepository<Department> getRepository() {
		return departmentRepository;
	}
	
	/**
	 * Save.
	 *
	 * @param department the department
	 * @return the department
	 * @throws DuplicatedException the duplicated exception
	 */
	/* (non-Javadoc)
	 * @see com.metasoft.em.service.AbstractService#save(org.springframework.data.jpa.domain.AbstractPersistable)
	 */
	public Department save(Department department) throws DuplicatedException {
		Department existingDepartment = 
					departmentRepository.findByDepartmentCode(department.getDepartmentCode());
		if (existingDepartment != null) {
			throw new DuplicatedException();
		}
		getRepository().save(department);
		return department;
	}
	
	/**
	 * Update.
	 *
	 * @param department the Department
	 * @return the t
	 * @throws NotFoundException the not found exception
	 * @throws DuplicatedException the duplicated exception
	 */
	public Department update(Department department) throws NotFoundException, DuplicatedException {
		Department existingEntity = getRepository().findOne(department.getId());
		if (existingEntity == null) {
			throw new NotFoundException();
		}
		if (!department.getDepartmentCode().equals(existingEntity.getDepartmentCode()) &&
				departmentRepository.findByDepartmentCode(department.getDepartmentCode()) != null) {
			throw new DuplicatedException();
		}
		
		//we don't set managerId and managerName as that one will be updated when
		// we create new or edit in EmployeeDepartment.
		existingEntity.setDepartmentCode(department.getDepartmentCode());
		existingEntity.setDepartmentName(department.getDepartmentName());
		existingEntity.setParentId(department.getParentId());
		
		getRepository().save(existingEntity);
		return existingEntity;
	}

}
