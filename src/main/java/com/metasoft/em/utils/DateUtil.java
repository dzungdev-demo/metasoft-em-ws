package com.metasoft.em.utils;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

/**
 * The Class DateUtil.
 *
 * @author Alex Dang
 */
public class DateUtil {
	
	/**
	 * Truncate time.
	 *
	 * @param date the date
	 * @return the date
	 */
	public static Date truncateTime(Date date) {
		return DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * Checks if is valid start date end date.
	 *
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return true, if is valid start date end date
	 */
	public static boolean isValidStartDateEndDate(Date startDate, Date endDate) {
		if (endDate != null &&
				endDate.before(startDate)) return false;
		return true;
	}

}
