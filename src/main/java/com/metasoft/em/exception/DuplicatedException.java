package com.metasoft.em.exception;

/**
 * The Class DuplicatedException.
 *
 * @author Alex Dang
 */
public class DuplicatedException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new duplicated exception.
	 */
	public DuplicatedException() {}

	/**
	 * Instantiates a new duplicated exception.
	 *
	 * @param message the message
	 */
	public DuplicatedException(String message) {
		super(message);
	}

}
