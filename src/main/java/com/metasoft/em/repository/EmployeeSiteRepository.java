package com.metasoft.em.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.metasoft.em.entity.EmployeeSite;


/**
 * The Interface EmployeeSiteRepository.
 *
 * @author Alex Dang
 */
public interface EmployeeSiteRepository extends IRepository<EmployeeSite> {

	/**
	 * Find by employee id and site id.
	 *
	 * @param employeeId the employee id
	 * @param siteId the site id
	 * @return the employee site
	 */
	public EmployeeSite findByEmployeeIdAndSiteId(Long employeeId, Long siteId);

	/**
	 * Find by employee id.
	 *
	 * @param employeeId the employee id
	 * @return the list
	 */
	public List<EmployeeSite> findByEmployeeId(Long employeeId);
	
	/**
	 * Delete by employee id and site id.
	 *
	 * @param employeeId the employee id
	 * @param siteId the site id
	 * @return the long
	 */
	public Long deleteByEmployeeIdAndSiteId(Long employeeId, Long siteId);
	
	/**
	 * Find overlapsed EmployeeSite
	 * 			SD1<=SD2<=ED1 OR SD1<=ED2<=ED1 OR SD2<=SD1<=ED2 OR SD2<=ED1<=ED2.
	 *
	 * @param employeeId the employee id
	 * @param siteId the site id
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return the list
	 */
	@Query("select es from EmployeeSite es where"
			+ " es.siteId = :siteId and es.employeeId = :employeeId "
			+ " and ("
			+ "     (es.startDate between :startDate and :endDate) or"
			+ "     (es.endDate between :startDate and :endDate) or"
			+ "		(:startDate between es.startDate and es.endDate) or"
			+ "		(:endDate between es.startDate and es.endDate)"
			+ " ) ")
	public List<EmployeeSite> findOverlapsedEmpSite(
											@Param("employeeId") Long employeeId,
											@Param("siteId") Long siteId, 
											@Param("startDate") Date startDate, 
											@Param("endDate") Date endDate);
	
	/**
	 * Find overlapsed emp dept except id.
	 *
	 * @param employeeId the employee id
	 * @param siteId the site id
	 * @param startDate the start date
	 * @param endDate the end date
	 * @param id the id
	 * @return the list
	 */
	@Query("select es from EmployeeSite es where"
			+ " es.siteId = :siteId and es.employeeId = :employeeId "
			+ "	and es.id <> :id "
			+ " and ("
			+ "     (es.startDate between :startDate and :endDate) or"
			+ "     (es.endDate between :startDate and :endDate) or"
			+ "		(:startDate between es.startDate and es.endDate) or"
			+ "		(:endDate between es.startDate and es.endDate)"
			+ " ) ")
	public List<EmployeeSite> findOverlapsedEmpSiteExceptId(
											@Param("employeeId") Long employeeId,
											@Param("siteId") Long siteId, 
											@Param("startDate") Date startDate, 
											@Param("endDate") Date endDate,
											@Param("id") Long id);
	
	
	
	
	/**
	 * Finding list of EmployeeSite which have startDate is 
	 * before the new StartDate and endDate of prev ED is null.
	 *
	 * @param employeeId the employee id
	 * @param siteId the site id
	 * @param startDate the start date
	 * @return List<EmployeeSite>
	 */
	
	@Query("select es from EmployeeSite es where"
			+ " es.siteId = :siteId and es.employeeId = :employeeId " 
			+ " and es.startDate < :startDate"
			+ " order by es.startDate desc")
	public List<EmployeeSite> findSameEmployeeSiteOrderByStartDateDesc(
											@Param("employeeId") Long employeeId,
											@Param("siteId") Long siteId ,
											@Param("startDate") Date startDate);
	
	
	/**
	 * Find first by employment id and site id order by start date desc.
	 *
	 * @param empId the emp id
	 * @param siteId the site id
	 * @param manager the manager
	 * @return the EmployeeSite
	 */
	public EmployeeSite 
				findFirstByEmployeeIdAndSiteIdOrderByStartDateDesc(
						Long empId, Long siteId, String manager);

}