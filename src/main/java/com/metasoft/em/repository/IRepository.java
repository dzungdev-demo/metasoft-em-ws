package com.metasoft.em.repository;

import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * The Interface IRepository.
 *
 * @author Alex Dang
 * @param <T> the generic type
 */
public interface IRepository<T extends AbstractPersistable<Long>>
		extends JpaRepository<T, Long>, JpaSpecificationExecutor<T> {

}
