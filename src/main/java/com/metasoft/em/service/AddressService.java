package com.metasoft.em.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metasoft.em.entity.Address;
import com.metasoft.em.repository.AddressRepository;
import com.metasoft.em.repository.IRepository;

/**
 * The Class AddressService.
 *
 * @author Alex Dang
 */
@Service
@Transactional
public class AddressService extends AbstractService<Address>{
	
	/** The address repository. */
	@Autowired
	private AddressRepository addressRepository;

	/* (non-Javadoc)
	 * @see com.metasoft.em.service.AbstractService#getRepository()
	 */
	@Override
	public IRepository<Address> getRepository() {
		return addressRepository;
	}

}
