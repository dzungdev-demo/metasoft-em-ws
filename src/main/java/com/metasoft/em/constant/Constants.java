package com.metasoft.em.constant;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 * The Class Constants.
 *
 * @author Alex Dang
 */
public final class Constants {
	
	/** The Constant EMPLOYEE_NO_PREFIX. */
	public static final String EMPLOYEE_NO_PREFIX = "EMP";
	
	/** The Constant EMPLOYEE_NO_FORMAT. */
	public static final DecimalFormat EMPLOYEE_NO_FORMAT = new DecimalFormat("00000");
	
	/** The Constant YYYY_MM_DD_PATTERN. */
	public static final String YYYY_MM_DD_PATTERN = "yyyy-MM-dd";
	
	/** The Constant MM_DD_YYYY_DATE_FORMAT. */
	public static final DateFormat MM_DD_YYYY_DATE_FORMAT = new SimpleDateFormat("MM-dd-yyyy");
	
	/** The Constant DD_MM_YYYY_DATE_FORMAT. */
	public static final DateFormat DD_MM_YYYY_DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
	
	/** The Constant YYYY_MM_DD_PATTERN_ARR. */
	public static final String[] YYYY_MM_DD_PATTERN_ARR = {YYYY_MM_DD_PATTERN};
	
	/** The Constant BOOLEAN_YES. */
	public static final String BOOLEAN_YES = "Y";
	
	/** The Constant BOOLEAN_NO. */
	public static final String BOOLEAN_NO = "N";
	
	/** The Constant COMMA. */
	public static final String COMMA = ",";
	
	/** The Constant LINE_BREAK. */
	public static final String LINE_BREAK = "\n";
	
	/** The Constant EMPLOYEE_TYPE_INTERNAL. */
	public static final String EMPLOYEE_TYPE_INTERNAL = "Internal";
	
	/** The Constant EMPLOYEE_TYPE_EXTERNAL. */
	public static final String EMPLOYEE_TYPE_EXTERNAL = "External";
	
	public static final String TEST = "test_jenkins";
	
}
