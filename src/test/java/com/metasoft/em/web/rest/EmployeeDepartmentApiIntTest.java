package com.metasoft.em.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.metasoft.em.AbstractIntegrationTest;
import com.metasoft.em.constant.Constants;
import com.metasoft.em.entity.Department;
import com.metasoft.em.entity.Employee;
import com.metasoft.em.entity.EmployeeDepartment;
import com.metasoft.em.repository.DepartmentRepository;
import com.metasoft.em.repository.EmployeeDepartmentRepository;
import com.metasoft.em.service.EmployeeDepartmentService;
import com.metasoft.em.service.EmployeeService;

/**
 * The Class EmployeeDepartmentApiIntTest.
 *
 * @author Alex Dang
 */
public class EmployeeDepartmentApiIntTest extends AbstractIntegrationTest {

	/** The rest employee api mock mvc. */
	private MockMvc restEmpDeptApiMocMvc;

	/** The employee service. */
	@Autowired
	private EmployeeDepartmentService empDeptService;
	
	/** The employee service. */
	@Autowired
	private EmployeeService employeeService;
	
	/** The department repository. */
	@Autowired
	private DepartmentRepository departmentRepository;
	
	/** The employee department repository. */
	@Autowired
	private EmployeeDepartmentRepository employeeDepartmentRepository;

	/**
	 * This method is used to initialize the Mock object for testing.
	 */
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		EmployeeDepartmentApi mockEmployeeDepartmentApi = new EmployeeDepartmentApi();
		ReflectionTestUtils.setField(mockEmployeeDepartmentApi, "employeeDepartmentService", empDeptService);

		restEmpDeptApiMocMvc = MockMvcBuilders.standaloneSetup(mockEmployeeDepartmentApi).build();
	}
	
	/**
	 * Test assignEmployeeToDepartment.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testAssignEmployeeToDepartmentForCMGRDepartment_HongToCMGR_thenSuccess() throws Exception {
		
		Employee hongEmp = employeeService.findOne(HONG_EMPLOYEE_ID);
		Department cmgrDept = departmentRepository.findByDepartmentCode(CMGR_DEPARTMENT_CODE);
		
		EmployeeDepartment hongIsAssignedToCMGR = new EmployeeDepartment(hongEmp, cmgrDept);
		hongIsAssignedToCMGR.setStartDate(new Date());
		hongIsAssignedToCMGR.setEndDate(DateUtils.parseDate("2018-01-01", Constants.YYYY_MM_DD_PATTERN_ARR));
		
		String urlPath = EMP_DEPARTMENT_BASE_PATH;
		restEmpDeptApiMocMvc.perform(post(urlPath)
							.contentType(MediaType.APPLICATION_JSON_VALUE)
							.content(json(hongIsAssignedToCMGR)))
							.andExpect(status().isCreated());
		
		assertThat(employeeService.isEmployeeAssignedToDepartmentAtDate(
							HONG_EMPLOYEE_NO, 
							CMGR_DEPARTMENT_CODE, 
							new Date())).isEqualTo(true);
		
	}
	
	
	/**
	 * Test find all employee department.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFindAllEmployeeDepartment() throws Exception {
		
		String empDeptUrlPath = EMP_DEPARTMENT_BASE_PATH;
	
		restEmpDeptApiMocMvc.perform(get(empDeptUrlPath)
							.accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
							.andExpect(status().isOk())
							.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
							.andExpect(jsonPath("$", hasSize(7)));
	}
	
	
	/**
	 * Test edit employee department assign hong to CMG R then OK.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testEditEmployeeDepartment_assignHongToCMGR_thenOK() throws Exception {
		
		Department cmgrDept = departmentRepository.findByDepartmentCode(CMGR_DEPARTMENT_CODE);
		
		Date startDate2008_01_01 = DateUtils.parseDate("2008-01-01", Constants.YYYY_MM_DD_PATTERN_ARR);
		Date endDate2009_04_20 = DateUtils.parseDate("2009-04-20", Constants.YYYY_MM_DD_PATTERN_ARR);
		
		EmployeeDepartment hongIsAssignedToCMGR = employeeDepartmentRepository.findOne(4L);
		hongIsAssignedToCMGR.setStartDate(startDate2008_01_01);
		hongIsAssignedToCMGR.setEndDate(endDate2009_04_20);
		hongIsAssignedToCMGR.setDepartment(cmgrDept);
		
		//EmployeeDepartment contains information that Hong is assigned to CMGO has id is 4 in test-data.sql
		String urlPath = EMP_DEPARTMENT_BASE_PATH.concat("/4");
		restEmpDeptApiMocMvc.perform(put(urlPath)
							.contentType(MediaType.APPLICATION_JSON_VALUE)
							.content(json(hongIsAssignedToCMGR)))
							.andExpect(status().isOk());
		
		EmployeeDepartment hongCMGR = 
					employeeDepartmentRepository.findByEmployeeIdAndDepartmentId(hongIsAssignedToCMGR.getEmployeeId(), 
																				 cmgrDept.getId());
		assertThat(hongCMGR.getStartDate()).isEqualTo(startDate2008_01_01);
		assertThat(hongCMGR.getEndDate()).isEqualTo(endDate2009_04_20);
		
	}
	
	/**
	 * Test edit employee department by id delete hong to CMGO id 4 then OK.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testEditEmployeeDepartmentById_deleteHongToCMGOId4_thenOK() throws Exception {
		
		EmployeeDepartment hongCMGO = employeeDepartmentRepository.findOne(4L);
		
		//EmployeeDepartment contains information that Hong is assigned to CMGO has id is 4 in test-data.sql
		String urlPath = EMP_DEPARTMENT_BASE_PATH.concat("/4");
		restEmpDeptApiMocMvc.perform(delete(urlPath)
							.contentType(MediaType.APPLICATION_JSON_VALUE)
							.content(json(hongCMGO)))
							.andExpect(status().isOk());
		
		hongCMGO =  employeeDepartmentRepository.findOne(4L);
		assertThat(hongCMGO).isNull();
		
	}
	
	/**
 * Creates the employee department for hongto CMG ofrom 2018 01 01 to 2022 03 03 then conflict.
 *
 * @throws IOException Signals that an I/O exception has occurred.
 * @throws Exception the exception
 */
@Test
	public void createEmployeeDepartment_forHongtoCMGOfrom2018_01_01to2022_03_03_thenConflict() throws IOException, Exception {
		Employee hongEmp = employeeService.findOne(HONG_EMPLOYEE_ID);
		Department cmgoDepartment = departmentRepository.findOne(CMGO_DEPARTMENT_ID);
		
		Date startDate2018_01_01 = DateUtils.parseDate("2018-01-01", Constants.YYYY_MM_DD_PATTERN_ARR);
		Date endDate2022_03_03 = DateUtils.parseDate("2022-03-03", Constants.YYYY_MM_DD_PATTERN_ARR);
		
		EmployeeDepartment hongCMGO2018_01_01To2022_03_03 = new EmployeeDepartment(hongEmp, cmgoDepartment);
		hongCMGO2018_01_01To2022_03_03.setStartDate(startDate2018_01_01);
		hongCMGO2018_01_01To2022_03_03.setEndDate(endDate2022_03_03);
		
		restEmpDeptApiMocMvc.perform(post(EMP_DEPARTMENT_BASE_PATH)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(json(hongCMGO2018_01_01To2022_03_03)))
				.andExpect(status().isConflict());
	}
}
