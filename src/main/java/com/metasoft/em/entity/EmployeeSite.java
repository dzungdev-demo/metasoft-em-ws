package com.metasoft.em.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * The Class EmployeeSite.
 *
 * @author Alex Dang
 */
@Entity
@Table(name="employee_site")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EmployeeSite.class)
public class EmployeeSite extends AbstractPersistable<Long>{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3168454398452883570L;

	/** The employee id. */
	@Column(name = "employee_id", nullable = false)
	private Long employeeId;

	/** The site id. */
	@NotNull
	@Column(name = "site_id", nullable = false)
	private Long siteId;
	
	/** The start date. */
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="start_date", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date startDate;
	
	/** The end date. */
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="end_date", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date endDate;

	/** The employee. */
	@ManyToOne
	@JoinColumn(name = "employee_id", referencedColumnName = "id", insertable = false, updatable = false)
	@JsonIgnore
	private Employee employee;

	/** The site. */
	@ManyToOne
	@JoinColumn(name = "site_id", referencedColumnName = "id", insertable = false, updatable = false)
	@JsonIgnore
	private Site site;

	/**
	 * Instantiates a new employee site.
	 */
	public EmployeeSite() {}

	/**
	 * Instantiates a new employee site.
	 *
	 * @param employee the employee
	 * @param site the site
	 */
	public EmployeeSite(Employee employee, Site site) {
		this.employee = employee;
		this.site = site;
		this.employeeId = employee.getId();
		this.siteId = site.getId();
	}

	/**
	 * Gets the employee id.
	 *
	 * @return the employee id
	 */
	public Long getEmployeeId() {
		return employeeId;
	}

	/**
	 * Sets the employee id.
	 *
	 * @param employeeId the new employee id
	 */
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * Gets the site id.
	 *
	 * @return the site id
	 */
	public Long getSiteId() {
		return siteId;
	}

	/**
	 * Sets the site id.
	 *
	 * @param siteId the new site id
	 */
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	/**
	 * Gets the employee.
	 *
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * Sets the employee.
	 *
	 * @param employee the new employee
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
		if (employee != null && !employee.isNew())
			this.employeeId = employee.getId();
	}

	/**
	 * Gets the site.
	 *
	 * @return the site
	 */
	public Site getSite() {
		return site;
	}

	/**
	 * Sets the site.
	 *
	 * @param site the new site
	 */
	public void setSite(Site site) {
		this.site = site;
		if (site != null && !site.isNew())
			this.siteId = site.getId();
	}

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Gets the full address.
	 *
	 * @return the full address
	 */
	public String getFullAddress() {
		return site == null? "" : site.getFullAddress();
	}
	
	/**
	 * Gets the site name.
	 *
	 * @return the site name
	 */
	public String getSiteName() {
		return site == null ? "" : site.getSiteName();
	}
}