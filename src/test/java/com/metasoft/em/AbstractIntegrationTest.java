package com.metasoft.em;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.metasoft.em.entity.Address;
import com.metasoft.em.entity.Department;
import com.metasoft.em.service.AddressService;
import com.metasoft.em.service.DepartmentService;

/**
 * The Class AbstractIntegrationTest.
 *
 * @author Alex Dang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EMApp.class)
@WebAppConfiguration
@IntegrationTest({ "server.port=0", "management.port=0" })
@Transactional
@TestPropertySource(locations = { "classpath:test-application.properties" })
public class AbstractIntegrationTest {
	
	/** The Constant CMGO_DEPARTMENT_ID. */
	public static final Long CMGO_DEPARTMENT_ID = 2L;
	
	/** The Constant STUTTGART_ADDRESS_ID. */
	public static final Long STUTTGART_ADDRESS_ID = 1L;
	
	/** The Constant HONG_EMPLOYEE_NO. */
	public static final String HONG_EMPLOYEE_NO = "EMP00001";
	
	/** The Constant HOO_EMPLOYEE_NO. */
	public static final String HOO_EMPLOYEE_NO = "EMP00002";
	
	/** The Constant DR_THED_EMPLOYEE_NO. */
	public static final String DR_THED_EMPLOYEE_NO = "EMP00003";
	
	/** The Constant CHEW_EMPLOYEE_NO. */
	public static final String CHEW_EMPLOYEE_NO = "EMP00004";
	
	/** The Constant WILSON_EMPLOYEE_NO. */
	public static final String WILSON_EMPLOYEE_NO = "EMP00005";
	
	/** The Constant HONG_EMPLOYEE_ID. */
	public static final Long HONG_EMPLOYEE_ID = 1L;
	
	/** The Constant CMGO_DEPARTMENT_CODE. */
	public static final String CMGO_DEPARTMENT_CODE = "CMGO";
	
	/** The Constant CMGR_DEPARTMENT_CODE. */
	public static final String CMGR_DEPARTMENT_CODE = "CMGR";
	
	/** The Constant CMGG_DEPARTMENT_CODE. */
	public static final String CMGG_DEPARTMENT_CODE = "CMGG";
	
	/** The Constant STUTTGART_SITE_NAME. */
	public static final String STUTTGART_SITE_NAME = "Stuttgart";
	
	/** The Constant ESSEN_SITE_NAME. */
	public static final String ESSEN_SITE_NAME = "Essen";
	
	/** The Constant HANOVER_SITE_NAME. */
	public static final String HANOVER_SITE_NAME = "Hanover";
	
	/** The Constant DEPARTMENT_API_BASE_PATH. */
	public static final String DEPARTMENT_API_BASE_PATH = "/api/v1/departments";
	
	/** The Constant SITE_API_BASE_PATH. */
	public static final String SITE_API_BASE_PATH = "/api/v1/sites";
	
	/** The Constant EMP_API_BASE_PATH. */
	public static final String EMP_API_BASE_PATH = "/api/v1/employees";
	
	/** The Constant EMP_DEPARTMENT_BASE_PATH. */
	public static final String EMP_DEPARTMENT_BASE_PATH = "/api/v1/employeeDepartments";
	
	/** The address service. */
	@Autowired
	protected AddressService addressService;
	
	/** The department service. */
	@Autowired
	protected DepartmentService departmentService;
	
	/** The address. */
	protected Address address;
	
	/** The cmgo department. */
	protected Department cmgoDepartment;
	
	/** The mapping jackson 2 http message converter. */
	protected HttpMessageConverter mappingJackson2HttpMessageConverter;
	
	/**
	 * Sets the converters.
	 *
	 * @param converters the new converters
	 */
	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(
                hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

    }
	
	/**
	 * Json.
	 *
	 * @param obj the obj
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected String json(Object obj) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
        		obj, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
	
	/**
	 * Setup.
	 */
	@Before
	public void setup() {
		this.address = addressService.findOne(STUTTGART_ADDRESS_ID);
		this.cmgoDepartment = departmentService.findOne(CMGO_DEPARTMENT_ID);
	}
	
}
