package com.metasoft.em.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metasoft.em.constant.Constants;
import com.metasoft.em.entity.Department;
import com.metasoft.em.entity.Employee;
import com.metasoft.em.entity.EmployeeDepartment;
import com.metasoft.em.entity.EmployeeSite;
import com.metasoft.em.exception.ConflictException;
import com.metasoft.em.exception.DuplicatedException;
import com.metasoft.em.exception.NotFoundException;
import com.metasoft.em.repository.AddressRepository;
import com.metasoft.em.repository.DepartmentRepository;
import com.metasoft.em.repository.EmployeeRepository;
import com.metasoft.em.repository.EmployeeSiteRepository;
import com.metasoft.em.repository.IRepository;
import com.metasoft.em.utils.DateUtil;

/**
 * The Class EmployeeService.
 *
 * @author Alex Dang
 */
@Service
@Transactional
public class EmployeeService extends AbstractService<Employee> {

	/** The employee repository. */
	@Autowired
	private EmployeeRepository employeeRepository;

	/** The department repository. */
	@Autowired
	private DepartmentRepository departmentRepository;

	/** The employee department repository. */
	@Autowired
	private EmployeeDepartmentService employeeDepartmentService;

	/** The employee site repository. */
	@Autowired
	private EmployeeSiteRepository employeeSiteRepository;
	
	/** The employee site service. */
	@Autowired
	private EmployeeSiteService employeeSiteService;

	/** The address repository. */
	@Autowired
	private AddressRepository addressRepository;

	/**
	 * This method is provide the IRepository for based method.
	 *
	 * @return the IRepository
	 */
	@Override
	public IRepository<Employee> getRepository() {
		return employeeRepository;
	}

	/**
	 * Find by employee no.
	 *
	 * @param employeeNo
	 *            the employee no
	 * @return the employee
	 */
	public Employee findByEmployeeNo(String employeeNo) {
		return employeeRepository.findByEmployeeNo(employeeNo);
	}

	/**
	 * This is method to create new employee.
	 *
	 * @param employee            the employee
	 * @param eds            the employee departments
	 * @param ess            the employee sites
	 * @return the saved employee which has Id
	 * @throws DuplicatedException             the duplicated exception
	 * @throws ParseException             the parse exception
	 * @throws ConflictException the conflict exception
	 */
	public Employee save(Employee employee, List<EmployeeDepartment> eds, List<EmployeeSite> ess) 
							throws DuplicatedException, ParseException, ConflictException {
		
		if (!DateUtil.isValidStartDateEndDate(employee.getEntryDate(), employee.getLeavingDate())) {
			throw new ConflictException(msg("entry.date.can.not.bigger.than.leaving.date"));
		}
		
		for (EmployeeSite es : ess) {
			if (!DateUtil.isValidStartDateEndDate(es.getStartDate(), es.getEndDate())) {
				throw new ConflictException(msg("start.date.can.not.bigger.than.end.date"));
			}
		}
		for (EmployeeDepartment ed : eds) {
			if (!DateUtil.isValidStartDateEndDate(ed.getStartDate(), ed.getEndDate())) {
				throw new ConflictException(msg("start.date.can.not.bigger.than.end.date"));
			}
		}
		
		if (employeeRepository.findBySsnOrEmail(employee.getSsn(), employee.getEmail()) != null) {
			throw new DuplicatedException(msg("employee.duplicate.ssn.email"));
		}
		
		if(employeeDepartmentService.isOverlapsed(eds)) {
			throw new ConflictException(msg("employee.department.period.overlapsed"));
		}
		
		if(employeeSiteService.isOverlapsed(ess)) {
			throw new ConflictException(msg("employee.site.period.overlapsed"));
		}
		
		if (!isValidEntryLeavingDateWithEmployeeSiteDepartmentDate(
											employee.getEntryDate(),
											employee.getLeavingDate(),
											eds,
											ess)) {
			throw new ConflictException(msg("employee.entry.date.leaving.date.conflict.ed.es.date"));
		}
			

		String employeeNo = createEmployeeNo();
		employee.setEmployeeNo(employeeNo);

		addressRepository.save(employee.getAddress());
		employeeRepository.save(employee);

		eds.forEach(ed -> ed.setEmployee(employee));
		ess.forEach(es -> es.setEmployee(employee));
		employee.setEmployeeDepartments(eds);
		employee.setEmployeeSites(ess);
		for (EmployeeDepartment employeeDepartment : eds) {
			employeeDepartmentService.save(employeeDepartment, true);
		}
		for (EmployeeSite employeeSite : ess) {
			employeeSiteRepository.save(employeeSite);
		}

		return employee;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.metasoft.em.service.AbstractService#update(org.springframework.data.
	 * jpa.domain.AbstractPersistable)
	 */
	public Employee update(Employee employee) throws NotFoundException, DuplicatedException, ConflictException {
		if (!DateUtil.isValidStartDateEndDate(employee.getEntryDate(), employee.getLeavingDate())) {
			throw new ConflictException(msg("entry.date.can.not.bigger.than.leaving.date"));
		}
		
		Employee existingEmployee = getRepository().findOne(employee.getId());
		if (existingEmployee == null) {
			throw new NotFoundException();
		}
		if ((!employee.getSsn().equals(existingEmployee.getSsn())
				&& employeeRepository.findBySsn(employee.getSsn()) != null)
				|| (!employee.getEmail().equals(existingEmployee.getEmail())
						&& employeeRepository.findByEmail(employee.getEmail()) != null)) {
			throw new DuplicatedException();
		}
		List<EmployeeDepartment> eds = employeeDepartmentService.findByEmployeeId(employee.getId());
		List<EmployeeSite> ess = employeeSiteService.findByEmployeeId(employee.getId());
		
		if (!isValidEntryLeavingDateWithEmployeeSiteDepartmentDate(
				employee.getEntryDate(),
				employee.getLeavingDate(),
				eds, ess)) {
			throw new ConflictException(msg("employee.entry.date.leaving.date.conflict.ed.es.date"));
		}

		BeanUtils.copyProperties(employee, existingEmployee);
		getRepository().save(existingEmployee);
		return existingEmployee;
	}

	/**
	 * Find current manager by department code.
	 *
	 * @param departmentCode
	 *            the department code
	 * @return the employee
	 */
	public Employee findCurrentManagerByDepartmentCode(String departmentCode) {
		Department department = departmentRepository.findByDepartmentCode(departmentCode);
		if (department == null || department.getManagerId() == null) {
			return null;
		}
		return findOne(department.getManagerId());
	}

	/**
	 * Find manager by department code at date.
	 *
	 * @param departmentCode
	 *            the department code
	 * @param atDate
	 *            the at date
	 * @return the employee
	 */
	public Employee findManagerByDepartmentCodeAtDate(String departmentCode, Date atDate) {
		List<Employee> employees = employeeRepository.findManagerByDepartmentCodeAtDate(departmentCode, atDate);
		return employees.isEmpty() ? null : employees.get(0);
	}

	/**
	 * Find current employees by department code.
	 *
	 * @param departmentCode
	 *            the department code
	 * @return the list
	 */
	public List<Employee> findCurrentEmployeesByDepartmentCode(String departmentCode) {
		return employeeRepository.findCurrentEmployeesByDepartmentCode(departmentCode);
	}

	/**
	 * Find employees by department code at date.
	 *
	 * @param departmentCode
	 *            the department code
	 * @param atDate
	 *            the at date
	 * @return the list
	 */
	public List<Employee> findEmployeesByDepartmentCodeAtDate(String departmentCode, Date atDate) {
		return employeeRepository.findEmployeesByDepartmentCodeAtDate(departmentCode, atDate);
	}

	/**
	 * Checks if is employee currently assigned to department.
	 *
	 * @param employeeNo
	 *            the employee no
	 * @param departmentCode
	 *            the department code
	 * @return true, if is employee currently assigned to department
	 */
	public boolean isEmployeeCurrentlyAssignedToDepartment(String employeeNo, String departmentCode) {
		return employeeRepository.findEmployeeCurrentlyAssignedToDepartment(employeeNo, departmentCode)
				.isEmpty() == false;
	}

	/**
	 * Checks if is employee assigned to department at date.
	 *
	 * @param employeeNo
	 *            the employee no
	 * @param departmentCode
	 *            the department code
	 * @param atDate
	 *            the at date
	 * @return true, if is employee assigned to department at date
	 */
	public boolean isEmployeeAssignedToDepartmentAtDate(String employeeNo, String departmentCode, Date atDate) {
		List<Employee> employees = employeeRepository.findSpecificEmployeeAssignedToDepartmentAtDate(employeeNo,
				departmentCode, atDate);
		return employees.isEmpty() == false;
	}

	/**
	 * Find current employees by site name.
	 *
	 * @param siteName
	 *            the site name
	 * @return the list
	 */
	public List<Employee> findCurrentEmployeesBySiteName(String siteName) {
		return employeeRepository.findEmployeesBySiteNameAtDate(siteName, new Date());
	}

	/**
	 * Find employees by site name at date.
	 *
	 * @param siteName
	 *            the site name
	 * @param atDate
	 *            the at date
	 * @return the list
	 */
	public List<Employee> findEmployeesBySiteNameAtDate(String siteName, Date atDate) {
		return employeeRepository.findEmployeesBySiteNameAtDate(siteName, atDate);
	}

	/**
	 * Creates the employee no.
	 *
	 * @return the string
	 * @throws ParseException
	 *             the parse exception
	 */
	private String createEmployeeNo() throws ParseException {
		Employee latestEmp = employeeRepository.findFirstByOrderByEmployeeNoDesc();

		double latestEmpNo = latestEmp == null ? 1
				: Constants.EMPLOYEE_NO_FORMAT
						.parse(latestEmp.getEmployeeNo().replace(Constants.EMPLOYEE_NO_PREFIX, ""))
						.doubleValue();
		return Constants.EMPLOYEE_NO_PREFIX.concat(Constants.EMPLOYEE_NO_FORMAT.format(++latestEmpNo));
	}

	/**
	 * Delete by primary key.
	 *
	 * @param id
	 *            the id
	 * @throws NotFoundException
	 *             the not found exception
	 */
	public void delete(Long id) throws NotFoundException {
		Employee employee = getRepository().findOne(id);
		if (employee == null) {
			throw new NotFoundException();
		}
		employeeDepartmentService.deleteDepartmentManagerInBatch(employee.getEmployeeDepartments());
		employeeRepository.delete(employee);
	}
	
	/**
	 * Checks if is valid entry leaving date with employee site department date.
	 *
	 * @param entryDate the entry date
	 * @param leavingDate the leaving date
	 * @param eds the eds
	 * @param ess the ess
	 * @return true, if is valid entry leaving date with employee site department date
	 */
	private boolean isValidEntryLeavingDateWithEmployeeSiteDepartmentDate(
						Date entryDate, Date leavingDate,
						List<EmployeeDepartment> eds, List<EmployeeSite> ess) {
		//sorting eds and ess to have the first small Start Date and compare with 
		//entryDate or leaving Date
		if (eds.isEmpty() || ess.isEmpty()) return false;
		eds.stream()
		   .sorted((ed1, ed2) -> ed1.getStartDate().compareTo(ed2.getStartDate()));
		if (entryDate.after(eds.get(0).getStartDate())) return false; 
		
		ess.stream()
		   .sorted((es1, es2) -> es1.getStartDate().compareTo(es2.getStartDate()));
		if (entryDate.after(ess.get(0).getStartDate())) return false;
		
		// leaving date
		if (leavingDate != null) {
			EmployeeDepartment lastED = eds.get(eds.size() - 1);
			if (lastED.getEndDate() == null || 
					leavingDate.before(lastED.getEndDate())) return false;
			EmployeeSite lastES = ess.get(ess.size() - 1);
			if (lastES.getEndDate() == null ||
					leavingDate.before(lastES.getEndDate())) return false;
		}
		
		return true;
	}
	
}
