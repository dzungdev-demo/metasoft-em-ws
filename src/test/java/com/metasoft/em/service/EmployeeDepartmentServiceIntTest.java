package com.metasoft.em.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.metasoft.em.AbstractIntegrationTest;
import com.metasoft.em.constant.Constants;
import com.metasoft.em.entity.Department;
import com.metasoft.em.entity.Employee;
import com.metasoft.em.entity.EmployeeDepartment;
import com.metasoft.em.exception.ConflictException;
import com.metasoft.em.exception.DuplicatedException;
import com.metasoft.em.utils.DateUtil;

/**
 * The Class EmployeeDepartmentServiceIntTest.
 *
 * @author Alex Dang
 */
public class EmployeeDepartmentServiceIntTest extends AbstractIntegrationTest {
	
	/** The employee department service. */
	@Autowired
	private EmployeeDepartmentService employeeDepartmentService;
	
	/** The employee service. */
	@Autowired
	private EmployeeService employeeService;
	
	/** The eds. */
	List<EmployeeDepartment> eds = new ArrayList<EmployeeDepartment>();
	
	/* (non-Javadoc)
	 * @see com.metasoft.em.AbstractIntegrationTest#setup()
	 */
	@Before
	public void setup() {
		eds.clear();
	}
	
	/**
	 * This method is used to test create new department and see that the 
	 * manager is store in department.
	 *
	 * @throws DuplicatedException the duplicated exception
	 * @throws ConflictException the conflict exception
	 * @throws ParseException the parse exception
	 */
	@Test 
	public void testSave() throws DuplicatedException, ConflictException, ParseException {
		//cmgo has current employee is Hoo Choseng: EMP00002
		//and now we will set Hong: EMP00001 to be manager of that department
		Employee emp00001NewManger = employeeService.findOne(1L);
		Department cmgoDepartment = departmentService.findOne(2L);
		EmployeeDepartment newEmpDept = new EmployeeDepartment(emp00001NewManger, cmgoDepartment);
		newEmpDept.setManager(true);
		// newManager will have startDate is today
		Date startDate2019_01_01 = DateUtils.parseDate("2019-01-01", Constants.YYYY_MM_DD_PATTERN_ARR);
		Date endDate2022_01_01 = DateUtils.parseDate("2022-01-01", Constants.YYYY_MM_DD_PATTERN_ARR);
		
		newEmpDept.setStartDate(startDate2019_01_01);
		newEmpDept.setEndDate(endDate2022_01_01);
		
		employeeDepartmentService.save(newEmpDept, false);

		EmployeeDepartment previousManagerEmployeeDepartment = employeeDepartmentService.findOne(2L);
		// when new manager have startDate is today, we expect previous manager
		// have endDate is yesterday
		Date oneDateBefore = DateUtil.truncateTime(DateUtils.addDays(startDate2019_01_01, -1));
		assertThat(previousManagerEmployeeDepartment.getEndDate()).isEqualTo(oneDateBefore);
		
		cmgoDepartment = departmentService.findOne(2L);
		assertThat(cmgoDepartment.getManagerId()).isEqualTo(1L);
		
	}
	
	/**
	 * ed1: 2014-01-01 -> 2014-09-01
	 * ed2: 2015-01-01 -> null
	 * ed3: 2014-09-02 -> null.
	 *
	 * @throws ParseException the parse exception
	 */
	
	@Test
	public void testIsOverlapsedForEmployeeDepartment_for3ElementsAnd2ndElementHasEndDateIsNull_thenIsOverlapsed() 
					throws ParseException {
		
		Date sd1_2014_01_01 = DateUtils.parseDate("2014-01-01", new String[] { Constants.YYYY_MM_DD_PATTERN });
		Date ed1_2014_09_01 = DateUtils.parseDate("2014-09-01", new String[] { Constants.YYYY_MM_DD_PATTERN });
		Date sd2_2015_01_01 = DateUtils.parseDate("2015-01-01", new String[] { Constants.YYYY_MM_DD_PATTERN });
		Date ed2_null = null;
		Date sd3_2014_09_02 = DateUtils.parseDate("2014-09-02", new String[] { Constants.YYYY_MM_DD_PATTERN });
		Date ed3_null = null;
		
		//case 1: ed1 is normal, ed2 has end date is null, ed3 is normal
		EmployeeDepartment ed1 = new EmployeeDepartment();
		ed1.setDepartmentId(1L);
		ed1.setStartDate(sd1_2014_01_01);
		ed1.setEndDate(ed1_2014_09_01);
		
		EmployeeDepartment ed2 = new EmployeeDepartment();
		ed2.setDepartmentId(1L);
		ed2.setStartDate(sd2_2015_01_01);
		ed2.setEndDate(ed2_null);
		
		EmployeeDepartment ed3 = new EmployeeDepartment();
		ed3.setDepartmentId(1L);
		ed3.setStartDate(sd3_2014_09_02);
		ed3.setEndDate(ed3_null);
		
		eds.add(ed1);
		eds.add(ed2);
		eds.add(ed3);
		
		assertThat(employeeDepartmentService.isOverlapsed(eds)).isEqualTo(true);
		
	}
	
	/**
	 * ed1: 2014-01-01 -> 2014-09-01
	 * ed2: 2015-01-01 -> null
	 * ed3: 2014-05-02 -> 2014-12-31.
	 *
	 * @throws ParseException the parse exception
	 */
	
	@Test
	public void testIsOverlapsedForEmployeeDepartment_OverlapBySartDateED3_thenIsOverlapsed() 
					throws ParseException {
		
		Date sd1_2014_01_01 = DateUtils.parseDate("2014-01-01", new String[] { Constants.YYYY_MM_DD_PATTERN });
		Date ed1_2014_09_01 = DateUtils.parseDate("2014-09-01", new String[] { Constants.YYYY_MM_DD_PATTERN });
		Date sd2_2015_01_01 = DateUtils.parseDate("2015-01-01", new String[] { Constants.YYYY_MM_DD_PATTERN });
		Date ed2_null = null;
		Date sd3_2014_05_02 = DateUtils.parseDate("2014-05-02", new String[] { Constants.YYYY_MM_DD_PATTERN });
		Date ed3_2014_12_31 = DateUtils.parseDate("2014-12-31", new String[] { Constants.YYYY_MM_DD_PATTERN });
		
		//case 1: ed1 is normal, ed2 has end date is null, ed3 is normal
		EmployeeDepartment ed1 = new EmployeeDepartment();
		ed1.setDepartmentId(1L);
		ed1.setStartDate(sd1_2014_01_01);
		ed1.setEndDate(ed1_2014_09_01);
		
		EmployeeDepartment ed2 = new EmployeeDepartment();
		ed2.setDepartmentId(1L);
		ed2.setStartDate(sd2_2015_01_01);
		ed2.setEndDate(ed2_null);
		
		EmployeeDepartment ed3 = new EmployeeDepartment();
		ed3.setDepartmentId(1L);
		ed3.setStartDate(sd3_2014_05_02);
		ed3.setEndDate(ed3_2014_12_31);
		
		eds.add(ed1);
		eds.add(ed2);
		eds.add(ed3);
		
		assertThat(employeeDepartmentService.isOverlapsed(eds)).isEqualTo(true);
	}

}
