package com.metasoft.em.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metasoft.em.entity.EmployeeDepartment;
import com.metasoft.em.exception.ConflictException;
import com.metasoft.em.exception.DuplicatedException;
import com.metasoft.em.exception.NotFoundException;
import com.metasoft.em.service.EmployeeDepartmentService;
import com.metasoft.em.web.dto.error.ErrorDTO;

/**
 * The Class EmployeeDepartmentApi.
 *
 * @author Alex Dang
 */
@RestController
@RequestMapping(value = "/api/v1/employeeDepartments")
public class EmployeeDepartmentApi {
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(EmployeeDepartmentApi.class);
	
	/** The employee department service. */
	@Autowired
	private EmployeeDepartmentService employeeDepartmentService;
	
	/**
	 * Find by id.
	 *
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<EmployeeDepartment>> findAll() {
		log.debug("start findAll for employeeDepartment");
		List<EmployeeDepartment> eds = employeeDepartmentService.findAll();
		if (eds == null) {
			log.debug("end findAll: NO_CONTENT:204 for employeeDepartment");
			return new ResponseEntity<List<EmployeeDepartment>>(HttpStatus.NO_CONTENT);
		}
		log.debug("end findAll: OK:204 for employeeDepartment");
		return new ResponseEntity<List<EmployeeDepartment>>(eds, HttpStatus.OK);
	}
	
	/**
	 * Find by id.
	 *
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<EmployeeDepartment> findById(@PathVariable("id") long id) {
		log.debug("start findById for employeeDepartment id {}", id);
		EmployeeDepartment employeeDepartment = employeeDepartmentService.findOne(id);
		if (employeeDepartment == null) {
			log.debug("end findById: NO_CONTENT:204 for employeeDepartment id {}", id);
			return new ResponseEntity<EmployeeDepartment>(HttpStatus.NO_CONTENT);
		}
		log.debug("end findById: OK:204 for employeeDepartment id {}", id);
		return new ResponseEntity<EmployeeDepartment>(employeeDepartment, HttpStatus.OK);
	}
	
	/**
	 * Creates the employee department.
	 *
	 * @param employeeDepartment the employee department
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> createEmployeeDepartment(
												@Validated @RequestBody EmployeeDepartment employeeDepartment) {
		log.debug("start createEmployeeDepartment for employee id {} and department id {}", 
					employeeDepartment.getEmployeeId(), employeeDepartment.getDepartmentId());
		try {
			employeeDepartmentService.save(employeeDepartment, false);
			log.debug("end createEmployeeDepartment: CREATED:201 for employee id {} and department id {}", 
					employeeDepartment.getEmployeeId(), employeeDepartment.getDepartmentId());
			return new ResponseEntity<Object>(employeeDepartment, HttpStatus.CREATED);
		} catch (DuplicatedException | ConflictException ex) {
			log.debug("end createEmployeeDepartment: CONFLICT:409 for employee id {} and department id {}", 
					employeeDepartment.getEmployeeId(), employeeDepartment.getDepartmentId());
			
			return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorDTO(ex.getMessage(), HttpStatus.CONFLICT.value()));
		}
	}
	
	/**
	 * Update employee department.
	 *
	 * @param id the id
	 * @param employeeDepartment the employee department
	 * @return the response entity
	 */
	@RequestMapping(value="/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateEmployeeDepartment(
										@PathVariable("id") Long id,
										@Validated @RequestBody EmployeeDepartment employeeDepartment) {
		log.debug("start updateEmployeeDepartment for employee id {} and department id {}", 
				employeeDepartment.getEmployeeId(), employeeDepartment.getDepartmentId());
		try {
			employeeDepartmentService.update(employeeDepartment);
			log.debug("end updateEmployeeDepartment  OK:200 for employee id {} and department id {}", 
					employeeDepartment.getEmployeeId(), employeeDepartment.getDepartmentId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} catch (NotFoundException nfe) {
			log.debug("end updateEmployeeDepartment for NOT_FOUND: 404 employee id {} and department id {}", 
					employeeDepartment.getEmployeeId(), employeeDepartment.getDepartmentId());
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorDTO(nfe.getMessage(), HttpStatus.NOT_FOUND.value()));
		} catch (DuplicatedException | ConflictException ex) {
			log.debug("end updateEmployeeDepartment CONFLICT:409 for employee id {} and department id {}", 
					employeeDepartment.getEmployeeId(), employeeDepartment.getDepartmentId());
			return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorDTO(ex.getMessage(), HttpStatus.CONFLICT.value()));
		} 
	}
	
	/**
	 * Delete employeeDepartment.
	 *
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<EmployeeDepartment> delete(@PathVariable("id") long id) {
		log.debug("start deleteEmployeeDepartment by employeeDepartment id {}", id);
		try {
			employeeDepartmentService.delete(id);
		} catch (NotFoundException nfe) {
			log.debug("end deleteEmployeeDepartment: NOT_FOUND:404 by employeeDepartment id {}", id);
			return new ResponseEntity<EmployeeDepartment>(HttpStatus.NOT_FOUND);
		}
		log.debug("end deleteEmployeeDepartment: OK:404 by employeeDepartment id {}", id);
		return new ResponseEntity<EmployeeDepartment>(HttpStatus.OK);
	}
	
}
